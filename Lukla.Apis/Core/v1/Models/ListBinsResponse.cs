﻿using System;

namespace Lukla.Apis.Core.v1.Models
{
    public class ListBinsResponse
    {
        public Guid Id { get; set; }
        public string BinId { get; set; }
        public string Name { get; set; }
    }
}
