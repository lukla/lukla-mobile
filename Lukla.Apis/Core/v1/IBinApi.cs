﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Lukla.Apis.Core.v1.Models;
using Refit;

namespace Lukla.Apis.Core.v1
{
    public interface IBinApi
    {
        [Get("/bins/list")]
        Task<List<ListBinsResponse>> ListBins();
    }
}
