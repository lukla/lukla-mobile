﻿using System;

namespace Lukla.Apis.Services
{
    public interface IApiFactory
    {
        T Generate<T>(Uri baseApiUri, string accessToken);
    }
}
