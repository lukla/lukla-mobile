﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mime;
using Refit;

namespace Lukla.Apis.Services
{
    public class ApiFactory : IApiFactory
    {
        public T Generate<T>(Uri baseApiUri, string accessToken)
        {
            var client = new HttpClient
            {
                BaseAddress = baseApiUri
            };

            if (!string.IsNullOrEmpty(accessToken))
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

            }

            return RestService.For<T>(client);
        }
    }
}
