﻿namespace Lukla.Mobile.Models
{
    public class VersionIdResponse
    {
        public string VersionId { get; set; }
        public string Message { get; set; }
        public string Errors { get; set; }
        public string Title { get; set; }
    }
}
