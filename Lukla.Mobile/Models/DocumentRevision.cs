﻿using System;

namespace Lukla.Mobile.Models
{
    public class DocumentRevision : BaseModel
    {
        public string DocumentNumber { get; set; }
        public bool IsDeleted { get; set; }
        public string FileName { get; set; }
        public DateTime DateUploaded { get; set; }
    }
}
