﻿using System.IO;

namespace Lukla.Mobile.Models
{
    //  todo: Move to mediator command
    public class UploadDocParams
    {
        public string FileName { get; set; }
        public string RequestJson { get; set; }
        public Stream File { get; set; }
    }
}
