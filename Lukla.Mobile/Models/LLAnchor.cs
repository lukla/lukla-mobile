﻿using System.Collections.Generic;
using Lukla.Mobile.Helpers;

namespace Lukla.Mobile.Models
{
    public class LLAnchor : BaseModel
    {
        public string InternalName { get; set; }
        public int DisplayOrder { get; set; }
        public bool UpdatedValueRequiredForNewVersion { get; set; }
        public bool IsMandatory { get; set; }
        public bool IsUnique { get; set; }
        public bool IsEditable { get; set; }
        public string Value { get; set; }
        public string DefaultValue { get; set; }
        public string InvalidCharacters { get; set; }
        public bool UsedInDocumentNumber { get; set; }
        public LLOptionChoice[] Choices { get; set; }
        public int Type { get; set; }
        public string ReferenceType { get; set; }

        public Dictionary<string, string> ToParamDict()
        {
            return new Dictionary<string, string>
            {
                { "Id", Id },
                { "Value", InputValue ?? "" }
            };
        }

        public bool IsChoiceType => ( Type == 1 || Type == 2 );
        public LLOptionChoice SelectedChoice { get; set; }
        public string InputValue
        {
            get
            {
                if (IsChoiceType)
                {
                    return SelectedChoice?.Abbreviation;
                }

                return !string.IsNullOrEmpty(Value) ? Value.Trim() : null;
            }
        }
        public bool IsValidInputValue()
        {
            var inputValue = InputValue;
            if (IsMandatory)
            {
                return Utils.IsValidFormValue(inputValue);
            }
            else
            {
                return inputValue == null || Utils.IsValidFormValue(inputValue);
            }
        }
    }
}
