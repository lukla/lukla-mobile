﻿using System.Collections.Generic;

namespace Lukla.Mobile.Models
{
    public class UserInfo : BaseModel
    {
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool IsActive { get; set; }
        public bool ReadOnly { get; set; }
        public string CompanyName { get; set; }
        public string CompanyId { get; set; }
        public List<string> Roles { get; set; }
        public string RolesString
        {
            get
            {
                if (Roles == null || Roles.Count == 0)
                {
                    return "";
                }
                return string.Join(",", Roles);
            }
        }
        public string FullName => $"{FirstName} {LastName}";
    }
}
