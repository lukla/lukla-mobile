﻿using System.Collections.Generic;
using Prism.Mvvm;

namespace Lukla.Mobile.Models
{
    public class IListResponse<T> : BindableBase
    {
        public List<T> Items { get; set; }
        public int TotalItems { get; set; }
        public int FilteredItems { get; set; }
    }
}
