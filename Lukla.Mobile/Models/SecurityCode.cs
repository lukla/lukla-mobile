﻿namespace Lukla.Mobile.Models
{
    public class SecurityCode : BaseModel
    {
        public string Code { get; set; }
        public int Rank { get; set; }
    }
}
