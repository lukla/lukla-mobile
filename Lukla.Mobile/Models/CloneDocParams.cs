﻿using System.Collections.Generic;

namespace Lukla.Mobile.Models
{
    //  Move to mediator Command
    public class CloneDocParams
    {
        public string BinId { get; set; }
        public string Title { get; set; }
        public string SecurityCodeId { get; set; }
        public Dictionary<string, string>[] Anchors { get; set; }
    }
}
