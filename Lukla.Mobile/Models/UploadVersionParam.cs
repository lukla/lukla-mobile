﻿
using System.IO;

namespace Lukla.Mobile.Models
{
    // TODO: Move to mediator command

    public class UploadVersionParam
    {
        public string FileName { get; set; }
        public string DocumentId { get; set; }
        public string Anchors { get; set; }
        public Stream File { get; set; }
    }
}
