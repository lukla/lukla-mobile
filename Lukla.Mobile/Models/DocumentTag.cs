﻿namespace Lukla.Mobile.Models
{
    public class DocumentTag : BaseModel
    {
        public string Value { get; set; }
    }
}
