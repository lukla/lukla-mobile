﻿using System.Net;

namespace Lukla.Mobile.Models
{
    public class LLApiResponse<T>
    {
        public HttpStatusCode StatusCode { get; set; }
        public string ErrorMessage { get; set; }
        public T Data { get; set; }

        public LLApiResponse(T data, HttpStatusCode statusCode = HttpStatusCode.OK,
            string errorMessage = null)
        {
            StatusCode = statusCode;
            ErrorMessage = errorMessage;
            Data = data;
        }

        public LLApiResponse()
        {
        }
    }
}
