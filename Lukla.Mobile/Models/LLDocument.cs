﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Lukla.Mobile.Models
{
    public class LLDocument : BaseModel
    {
        [JsonProperty("documentId")]
        public new string Id { get; set; }

        public string CompanyId { get; set; }
        public string VersionId { get; set; }
        public string DocumentNumber { get; set; }
        public string Title { get; set; }
        public string ProjectName { get; set; }
        public string ProjectId { get; set; }
        public string FileName { get; set; }
        public bool IsLocked { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime DateUploaded { get; set; }
        public int SecurityRank { get; set; }
        public DocumentTag[] Tags { get; set; }
        public DocumentRevision[] Revisions { get; set; }

        public List<LLAnchor> TagsToFilledValueAnchors(LLAnchor[] anchors)
        {
            var result = new List<LLAnchor>();
            if (Tags != null)
            {
                foreach (LLAnchor anchor in anchors)
                {
                    DocumentTag tag = Array.Find(Tags, e => e.Name == anchor.Name);
                    if (tag != null)
                    {
                        // copy value from tag to anchor
                        anchor.Value = tag.Value;
                    }
                    if (!string.IsNullOrEmpty(anchor.DefaultValue))
                    {
                        // set default value
                        anchor.Value = anchor.DefaultValue;
                    }
                    if (anchor.IsChoiceType)
                    {
                        anchor.SelectedChoice = Array.Find(anchor.Choices, e => e.Abbreviation == anchor.Value);
                    }
                    result.Add(anchor);
                }
            }
            return result;
        }
    }
}
