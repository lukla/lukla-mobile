﻿using System.IO;

namespace Lukla.Mobile.Models
{
    public class LLFile
    {
        public string FileName { get; set; }
        public Stream Stream { get; set; }
    }
}
