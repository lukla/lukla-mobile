﻿using System.Collections.Generic;

namespace Lukla.Mobile.Models
{
    //  TODO: Move to mediator command
    public class EditDocParams
    {
        public string DocumentId { get; set; }
        public string Title { get; set; }
        public string SecurityCodeId { get; set; }
        public Dictionary<string, string>[] Anchors { get; set; }
    }
}
