﻿using Prism.Mvvm;

namespace Lukla.Mobile.Models
{
    public class BaseModel : BindableBase
    {
        public string Id { get; set; }
        public string Name { get; set; }
        private bool _isSelected;
        public bool IsSelected
        {
            get => _isSelected;
            set => SetProperty(ref _isSelected, value);
        }
        public BaseModel()
        {
            IsSelected = false;
        }
    }
}
