﻿namespace Lukla.Mobile.Models
{
    //  TODO: Move to mediator 
    public class DocumentSearchParams
    {
        public string SearchTerm { get; set; }
        public string BinId { get; set; }
        public int Page { get; set; }
        //  TODO: Magic Number, move to configuration
        public int PageSize { get => 10; }
        public DocumentSearchParams()
        {
           Reset();
        }

        public void Reset()
        {
            Page = 1;
            SearchTerm = string.Empty;
            BinId = string.Empty;
        }
    }
}
