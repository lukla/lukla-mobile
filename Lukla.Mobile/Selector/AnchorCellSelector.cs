﻿using Lukla.Mobile.Models;
using Xamarin.Forms;

namespace Lukla.Mobile.Selector
{
    public class AnchorCellSelector : DataTemplateSelector
    {
        public DataTemplate IsTextCell { get; set; }
        public DataTemplate IsChoiceCell { get; set; }

        protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
        {
            return ((LLAnchor)item).IsChoiceType ? IsChoiceCell : IsTextCell;
        }
    }
}
