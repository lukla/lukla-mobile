﻿using System;
using System.Linq;
using DryIoc;
using Lukla.Mobile.Services;
using Lukla.Mobile.Services.Api;
using Lukla.Mobile.Services.Identity;
using Lukla.Mobile.Services.Mediator;
using Lukla.Mobile.Services.Mediator.Authentication;
using Lukla.Mobile.Services.Mediator.Behaviors;
using Lukla.Mobile.Services.Obsolute;
using Lukla.Mobile.Services.RequestProvider;
using Lukla.Mobile.Services.Settings;
using Lukla.Mobile.Services.Startup;
using Lukla.Mobile.ViewModels;
using Lukla.Mobile.Views;
using MediatR;
using Prism;
using Prism.DryIoc;
using Prism.Ioc;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace Lukla.Mobile
{
    public partial class App
    {
        /* 
         * The Xamarin Forms XAML Previewer in Visual Studio uses System.Activator.CreateInstance.
         * This imposes a limitation in which the App class must have a default constructor. 
         * App(IPlatformInitializer initializer = null) cannot be handled by the Activator.
         */
        public App() : this(null) { }

        public App(IPlatformInitializer initializer) : base(initializer) { }

        protected override async void OnInitialized()
        {
            InitializeComponent();
            await NavigationService.NavigateAsync("NavigationPage/TestView");



            //var hasLoggedIn = Application.Current.Properties.ContainsKey("access_token")
            //    && !string.IsNullOrEmpty((string)Application.Current.Properties["access_token"]);

            //if (hasLoggedIn)
            //{
            //    await NavigationService.NavigateAsync("NavigationPage/MainPage");
            //}
            //else
            //{
            //    await NavigationService.NavigateAsync("NavigationPage/Authen");
            //}
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            var container = containerRegistry.GetContainer()
                .RegisterMediator()
                .RegisterSettings()
                .RegisterLogger();

            containerRegistry.RegisterForNavigation<NavigationPage>();
            containerRegistry.RegisterForNavigation<MainPage, MainPageViewModel>();
            containerRegistry.RegisterForNavigation<Setting, SettingViewModel>();
            containerRegistry.RegisterForNavigation<Authen, AuthenViewModel>();
            containerRegistry.RegisterForNavigation<Logout, LogoutViewModel>();
            containerRegistry.RegisterForNavigation<UploadVersion, UploadVersionViewModel>();
            containerRegistry.RegisterSingleton<IIdentityService, IdentityService>();
            containerRegistry.RegisterSingleton<ISettingsService, SettingsService>();
            containerRegistry.RegisterSingleton<IRequestProvider, RequestProvider>();
            containerRegistry.RegisterSingleton<IApiManager, ApiManager>();
            containerRegistry.RegisterSingleton<IAuthenticator, Authenticator>();
            containerRegistry.RegisterForNavigation<SwitchCompany, SwitchCompanyViewModel>();
            containerRegistry.RegisterForNavigation<EditDocInfo, EditDocInfoViewModel>();
            containerRegistry.RegisterForNavigation<CloneDocument, CloneDocumentViewModel>();

            containerRegistry.RegisterSingleton<IMobileApiFactory, MobileApiFactory>();
            containerRegistry.RegisterSingleton<ICurrentSession, CurrentSession>();
            containerRegistry.RegisterForNavigation<TestView, TestViewModel>();
        }

    }

    public static class ContainerExtensions
    {
        public static bool IsMediatorHandler(this Type arg)
        {
            //Ugly hack to limit assembly registration to request handlers only
            return arg.GetInterfaces().Any(i => i.Name.StartsWith("IRequestHandler"));
        }
    }

}
