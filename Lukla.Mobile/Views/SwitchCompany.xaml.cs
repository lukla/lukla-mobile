﻿using Lukla.Mobile.Models;
using Lukla.Mobile.ViewModels;
using Xamarin.Forms;

namespace Lukla.Mobile.Views
{
    public partial class SwitchCompany : ContentPage
    {
        public SwitchCompany()
        {
            InitializeComponent();
            NavigationPage.SetBackButtonTitle(this, "");
        }

        void ListView_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            var viewModel = (SwitchCompanyViewModel)BindingContext;
            var company = (LLCompany)e.Item;
            if (viewModel != null && company != null)
            {
                viewModel.SelectedCompany = company;
            }
        }
    }
}
