﻿using Lukla.Mobile.ViewModels;
using Xamarin.Forms;

namespace Lukla.Mobile.Views
{
    public partial class Authen : ContentPage
    {
        public Authen()
        {
            InitializeComponent();
        }

        void WebView_Navigating(object sender, WebNavigatingEventArgs e)
        {
            var viewModel = (AuthenViewModel)BindingContext;
            if (viewModel == null) return;

            viewModel.IsBusy = true;
            _ = viewModel.NavigateAsync(e.Url);
        }

        void WebView_Navigated(object sender, WebNavigatedEventArgs e)
        {
            var viewModel = (AuthenViewModel)BindingContext;
            if (viewModel == null) return;

            var unescapedUrl = System.Net.WebUtility.UrlDecode(e.Url);
            if (unescapedUrl.Contains(GlobalSetting.Instance.Callback))
            {
                if (Device.RuntimePlatform == Device.Android)
                {
                    viewModel.ShowWebView = false;
                }
            }
            else
            {
                viewModel.IsBusy = false;
            }
        }
    }
}
