﻿using System;
using Lukla.Mobile.ViewModels;
using Xamarin.Forms;

namespace Lukla.Mobile.Views
{
    public partial class Logout : ContentPage
    {
        public Logout()
        {
            InitializeComponent();
        }

        void WebView_Navigating(System.Object sender, Xamarin.Forms.WebNavigatingEventArgs e)
        {
            Console.WriteLine("WebView_Navigating");
            var viewModel = (LogoutViewModel)BindingContext;
            if (viewModel != null)
            {
                viewModel.IsBusy = true;
            }
        }

        void WebView_Navigated(System.Object sender, Xamarin.Forms.WebNavigatedEventArgs e)
        {
            Console.WriteLine("WebView_Navigated");
            var viewModel = (LogoutViewModel)BindingContext;
            if (viewModel != null)
            {
                viewModel.IsBusy = false;
            }
        }
    }
}
