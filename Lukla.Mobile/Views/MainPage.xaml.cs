﻿using Lukla.Mobile.Models;
using Lukla.Mobile.ViewModels;
using Xamarin.Forms;

namespace Lukla.Mobile.Views
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            NavigationPage.SetBackButtonTitle(this, "");
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            var viewModel = (MainPageViewModel)BindingContext;
            if (viewModel == null) return;
            if (viewModel.Bins.Count != 0 && !viewModel.SettingsService.NeedRefeshBins) return;

            viewModel.TotalDocs = 0;
            _ = viewModel.InitialRequests();
        }

        void ListView_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            var viewModel = (MainPageViewModel)BindingContext;
            var doc = (LLDocument)e.Item;
            if (viewModel != null && doc != null)
            {
                viewModel.OnSelectDoc(doc);
            }
        }
    }
}