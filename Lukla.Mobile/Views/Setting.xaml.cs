﻿using Xamarin.Forms;

namespace Lukla.Mobile.Views
{
    public partial class Setting : ContentPage
    {
        public Setting()
        {
            InitializeComponent();
            NavigationPage.SetBackButtonTitle(this, "");
        }
    }
}
