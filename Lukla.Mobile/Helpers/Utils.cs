﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using Lukla.Mobile.Models;

namespace Lukla.Mobile.Helpers
{
    public static class Utils
    {
        public static bool IsValidFormValue(string text)
        {
            if (string.IsNullOrEmpty(text)) { return false; }
            if (text.Trim().Length == 0) { return false; }
            Regex r = new Regex(@"[\\\/%&?!-]");
            bool containsSpecialChars = r.IsMatch(text);
            return !containsSpecialChars;
        }

        public static string RandomString(int size)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }
            return builder.ToString();
        }

        public static Dictionary<string, string>[] ToArrayAnchorDict(List<LLAnchor> anchors)
        {
            var anchorsJson = new List<Dictionary<string, string>>();
            anchors.ForEach(anchor =>
            {
                if (!string.IsNullOrEmpty(anchor.InputValue))
                {
                    anchorsJson.Add(anchor.ToParamDict());
                }
            });
            return anchorsJson.ToArray();
        }

        public static LLOptionChoice[] BinsToChoices(List<LLBin> bins)
        {
            var result = new List<LLOptionChoice>();
            if (bins != null && bins.Count > 0)
            {
                foreach(LLBin bin in bins)
                {
                    var choice = new LLOptionChoice
                    {
                        Id = bin.BinId,
                        Abbreviation = bin.BinId,
                        Name = bin.Name
                    };
                    result.Add(choice);
                }
            }
            return result.ToArray();
        }
    }
}
