﻿using System;
using System.Net.Http;
using Fusillade;

namespace Lukla.Mobile.Services.Api
{
    public interface IMobileApiFactory
    {
        T Create<T>(Uri baseApiUri, string accessToken, Priority priority = Priority.UserInitiated);

        HttpClient Create(Priority priority = Priority.UserInitiated);
    }
}
