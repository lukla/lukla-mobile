﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using Fusillade;
using ModernHttpClient;
using Refit;

namespace Lukla.Mobile.Services.Api
{
    public class MobileApiFactory : IMobileApiFactory
    {
        public T Create<T>(Uri baseApiUri, string accessToken, Priority priority = Priority.UserInitiated)
        {
            var client = new HttpClient(new RateLimitedHttpMessageHandler(new NativeMessageHandler(), priority))
            {
                BaseAddress = baseApiUri
            };

            if (!string.IsNullOrEmpty(accessToken))
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            }

            return RestService.For<T>(client);
        }

        public HttpClient Create(Priority priority = Priority.UserInitiated)
        {
            return new HttpClient(new RateLimitedHttpMessageHandler(new NativeMessageHandler(), priority));
        }
    }
}
