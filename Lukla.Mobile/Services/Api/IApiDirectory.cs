﻿using System;

namespace Lukla.Mobile.Services.Api
{
    public interface IApiDirectory
    {
        Uri BinApiBaseUrl { get; set; }
    }
}
