﻿using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Lukla.Mobile.Services.Mediator.Authentication;
using MediatR;
using Refit;

namespace Lukla.Mobile.Services.Mediator.Behaviors
{
    public class TokenBehavior<TRequest, TResponse>
        : IPipelineBehavior<TRequest, TResponse>
        where TRequest : ITokenAction
    {
        private readonly IMediator _mediator;

        public TokenBehavior(IMediator mediator)
        {
            _mediator = mediator;
        }

        public async Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<TResponse> next)
        {
            try
            {
                return await next();
            }
            catch (ApiException err)
            {
                if (err.StatusCode == HttpStatusCode.Unauthorized)
                {
                    await _mediator.Send(new StartSession.Command(), cancellationToken);
                }
                
                throw;
            }
        }
    }
}
