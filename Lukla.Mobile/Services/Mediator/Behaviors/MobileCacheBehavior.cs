﻿using System;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Tasks;
using Akavache;
using MediatR;

namespace Lukla.Mobile.Services.Mediator.Behaviors
{
    public class MobileCacheBehavior<TRequest, TResponse>
        : IPipelineBehavior<TRequest, TResponse>
        where TRequest : IMobileCachingBehavior
    {
        public async Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<TResponse> next)
        {
            var cache = BlobCache.LocalMachine;
            var cachedItems = cache.GetAndFetchLatest(nameof(request), () => next(), offset =>
            {
                var elapsed = DateTimeOffset.Now - offset;
                return elapsed > new TimeSpan(hours: 24, minutes: 0, seconds: 0);
            });

            return await cachedItems.FirstOrDefaultAsync();
        }
    }
}
