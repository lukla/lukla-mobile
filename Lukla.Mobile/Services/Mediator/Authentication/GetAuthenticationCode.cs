﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using IdentityModel;
using Lukla.Mobile.Helpers;
using MediatR;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace Lukla.Mobile.Services.Mediator.Authentication
{
    public static class GetAuthenticationCode
    {
        public class Request : IRequest<Response> { }

        public class Handler : IRequestHandler<Request, Response>
        {
            private readonly IAuthenticationSettings _settings;
            private readonly IAuthenticator _authenticator;
            private readonly ILogger<Handler> _logger;

            public Handler(
                IAuthenticationSettings settings,
                IAuthenticator authenticator,
                ILogger<Handler> logger)
            {
                _settings = settings;
                _authenticator = authenticator;
                _logger = logger;
            }

            public async Task<Response> Handle(Request request, CancellationToken cancellationToken)
            {
                //  Create Code Verification
                var verifier = Utils.RandomString(128);
                var sHa256 = SHA256.Create();
                var challengeBytes = sHa256.ComputeHash(Encoding.UTF8.GetBytes(verifier));
                var code = Base64Url.Encode(challengeBytes);

                //  Create Querystring Parameters
                var querystringParameters = new Dictionary<string, string>
                {
                    {"client_id", _settings.ClientId},
                    {"client_secret", _settings.ClientSecret},
                    {"response_type", "code"},
                    {"access_type", "offline"},
                    {"scope", _settings.Scope},
                    {"redirect_uri", _settings.CallbackUrl.AbsoluteUri},
                    {"nonce", Guid.NewGuid().ToString("N")},
                    {"code_challenge", code},
                    {"code_challenge_method", "S256"},
                    {"grant_type", "authorization_code"},
                    {"state", Guid.NewGuid().ToString("N")}
                };

                var queryString = string.Join("&", querystringParameters.Select(kvp =>
                    $"{WebUtility.UrlEncode(kvp.Key)}={WebUtility.UrlEncode(kvp.Value)}").ToArray());
                var url = new Uri($"{_settings.AuthorizeEndpoint}?{queryString}");

                _logger.LogDebug($"Getting Authentication Code with url: {url}");
                var sessionResponse = await _authenticator.Authenticate(url, _settings.CallbackUrl);
                _logger.LogDebug($"Code Retrieved: {JsonConvert.SerializeObject(sessionResponse, Formatting.Indented)}");

                return new Response
                {
                    Code = sessionResponse.Properties.FirstOrDefault(x => x.Key == "code").Value,
                    Verifier = verifier
                };
            }
        }

        public class Response
        {
            public string Code { get; set; }
            public string Verifier { get; set; }
        }
    }
}
