﻿using System.Reactive.Linq;
using System.Threading;
using System.Threading.Tasks;
using Akavache;

namespace Lukla.Mobile.Services.Mediator.Authentication
{
    public class CurrentSession : ICurrentSession
    {
        private const string Key = "current-session";

        public async Task<ISession> Get(CancellationToken cancellationToken)
        {
            var cache = BlobCache.Secure;
            return await cache.GetObject<ISession>(Key);
        }

        public void Set(ISession session)
        {
            var cache = BlobCache.Secure;
            cache.InsertObject(Key, session);
        }

        public void Clear()
        {
            var cache = BlobCache.Secure;
            cache.Invalidate(Key);
        }
    }
}