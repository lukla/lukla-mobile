﻿using System.Threading;
using System.Threading.Tasks;

namespace Lukla.Mobile.Services.Mediator.Authentication
{
    public interface ICurrentSession
    {
        Task<ISession> Get(CancellationToken cancellationToken);
        void Set(ISession session);
        void Clear();
    }
}
