﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Lukla.Mobile.Services.Api;
using MediatR;

namespace Lukla.Mobile.Services.Mediator.Authentication
{
    public static class RevokeToken
    {
        public class Command : IRequest
        {
            public string AccessToken { get; }
            public Command(string accessToken) => AccessToken = accessToken;
        }

        public class Handler : IRequestHandler<Command>
        {
            private readonly IMobileApiFactory _factory;
            private readonly IAuthenticationSettings _settings;

            public Handler(
                IMobileApiFactory factory, 
                IAuthenticationSettings settings)
            {
                _factory = factory;
                _settings = settings;
            }

            public async Task<Unit> Handle(Command command, CancellationToken cancellationToken)
            {
                using (var client = _factory.Create())
                {
                    var basicAuth = _settings.ClientId + ':' + _settings.ClientSecret;

                    var plainTextBytes = Encoding.UTF8.GetBytes(basicAuth);
                    client.DefaultRequestHeaders.Authorization =
                        new AuthenticationHeaderValue("Basic", Convert.ToBase64String(plainTextBytes));

                    var request = new HttpRequestMessage(HttpMethod.Post, _settings.RevokeEndpoint);
                    var keyValues = new List<KeyValuePair<string, string>>
                    {
                        new KeyValuePair<string, string>("token", command.AccessToken),
                        new KeyValuePair<string, string>("token_type_hint", "access_token")
                    };

                    request.Content = new FormUrlEncodedContent(keyValues);
                    var response = await client.SendAsync(request, cancellationToken);
                    response.EnsureSuccessStatusCode();
                    return Unit.Value;
                }
            }
        }
    }
}
