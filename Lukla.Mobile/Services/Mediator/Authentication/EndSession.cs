﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Lukla.Mobile.Services.Api;
using MediatR;
using Microsoft.Extensions.Logging;

namespace Lukla.Mobile.Services.Mediator.Authentication
{
    public static class EndSession
    {
        public class Command : IRequest { }

        public class Handler : IRequestHandler<Command>
        {
            private readonly ICurrentSession _currentSession;
            private readonly IMediator _mediator;
            private readonly ILogger<Handler> _logger;

            public Handler(
                ICurrentSession currentSession, 
                IMediator mediator, 
                ILogger<Handler> logger)
            {
                _currentSession = currentSession;
                _mediator = mediator;
                _logger = logger;
            }
            
            public async Task<Unit> Handle(Command request, CancellationToken cancellationToken)
            {
                _logger.LogInformation($"End Session");
                var session = await _currentSession.Get(cancellationToken);
                await _mediator.Send(new RevokeToken.Command(session.AccessToken), cancellationToken);
                _currentSession.Clear();
                _logger.LogInformation($"Session Ended");
                return Unit.Value;
            }
        }
    }
}
