﻿namespace Lukla.Mobile.Services
{
    public interface ISession
    {
        public string AccessToken { get; set; }
    }
}
