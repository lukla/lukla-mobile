﻿using System;
using System.Threading.Tasks;
using Xamarin.Essentials;

namespace Lukla.Mobile.Services.Mediator.Authentication
{
    public interface IAuthenticator
    {
         Task<WebAuthenticatorResult> Authenticate(Uri url, Uri callbackUrl);
    }

    public class Authenticator : IAuthenticator
    {
        public Task<WebAuthenticatorResult> Authenticate(Uri url, Uri callbackUrl) =>
            WebAuthenticator.AuthenticateAsync(url, callbackUrl);
    }
}
