﻿using System;

namespace Lukla.Mobile.Services.Mediator.Authentication
{
    public class AuthenticationSettings : IAuthenticationSettings
    {
        public Uri AuthorizeEndpoint { get; set; }
        public Uri CallbackUrl { get; set; }
        public Uri TokenEndpoint { get; set; }
        public Uri RevokeEndpoint { get; set; }
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
        public string Scope { get; set; }
    }
}