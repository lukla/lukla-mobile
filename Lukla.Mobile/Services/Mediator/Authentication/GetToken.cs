﻿using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using Lukla.Mobile.Services.Api;
using MediatR;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Lukla.Mobile.Services.Mediator.Authentication
{
    public static class GetToken
    {
        public class Request : IRequest<Response>
        {
            public string Code { get; set; }
            public string CodeVerification { get; set; }
        }

        public class Handler : IRequestHandler<Request, Response>
        {
            private readonly IAuthenticationSettings _settings;
            private readonly ILogger<Handler> _logger;
            private readonly IMobileApiFactory _mobileApiFactory;

            public Handler(
                IAuthenticationSettings settings,
                ILogger<Handler> logger,
                IMobileApiFactory mobileApiFactory)
            {
                _settings = settings;
                _logger = logger;
                _mobileApiFactory = mobileApiFactory;
            }


            public async Task<Response> Handle(Request request, CancellationToken cancellationToken)
            {
                using (var client = _mobileApiFactory.Create())
                {
                    var tokenRequest =
                        @$"grant_type=authorization_code&code={request.Code}&redirect_uri={WebUtility.UrlEncode(_settings.CallbackUrl.AbsoluteUri)}&code_verifier={request.CodeVerification}&client_id={GlobalSetting.Instance.ClientId}&client_secret={GlobalSetting.Instance.ClientSecret}";

                    _logger.LogDebug($"Fetching Token: {_settings.TokenEndpoint} with data {tokenRequest}");
                    var requestContent = new StringContent(tokenRequest);
                    requestContent.Headers.ContentType = new MediaTypeHeaderValue("application/x-www-form-urlencoded");

                    var tokenResponse = await client.PostAsync(_settings.TokenEndpoint, requestContent, cancellationToken);
                    var content = await tokenResponse.Content.ReadAsStringAsync();
                    tokenResponse.EnsureSuccessStatusCode();
                    var response = JsonConvert.DeserializeObject<Response>(content, new JsonSerializerSettings
                    {
                        ContractResolver = new CamelCasePropertyNamesContractResolver(),
                        DateTimeZoneHandling = DateTimeZoneHandling.Utc,
                        NullValueHandling = NullValueHandling.Ignore
                    });
                    _logger.LogDebug($"Retrieved Token: {JsonConvert.SerializeObject(response, Formatting.Indented)}");
                    return response;
                }
            }
        }

        public class Response
        {
            [JsonProperty("id_token")]
            public string IdToken { get; set; }

            [JsonProperty("access_token")]
            public string AccessToken { get; set; }

            [JsonProperty("expires_in")]
            public int ExpiresIn { get; set; }

            [JsonProperty("token_type")]
            public string TokenType { get; set; }

            [JsonProperty("refresh_token")]
            public string RefreshToken { get; set; }

            [JsonProperty("scope")]
            public string Scope { get; set; }
        }
    }
}
