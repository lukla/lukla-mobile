﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Logging;

namespace Lukla.Mobile.Services.Mediator.Authentication
{
    public static class StartSession
    {
        public class Command : IRequest { }

        public class Handler : IRequestHandler<Command>
        {
            private readonly ILogger<Handler> _logger;
            private readonly IMediator _mediator;
            private readonly ICurrentSession _currentSession;

            public Handler(
                ILogger<Handler> logger,
                IMediator mediator, 
                ICurrentSession currentSession)
            {
                _logger = logger;
                _mediator = mediator;
                _currentSession = currentSession;
            }

            public async Task<Unit> Handle(Command command, CancellationToken cancellationToken)
            {
                _logger.LogInformation($"Start Session");
                var codeResponse = await _mediator.Send(new GetAuthenticationCode.Request(), cancellationToken);
                var tokenResponse = await _mediator.Send(new GetToken.Request()
                {
                    Code = codeResponse.Code,
                    CodeVerification = codeResponse.Verifier
                }, cancellationToken);
                
                _currentSession.Set(new Session
                {
                    AccessToken = tokenResponse.AccessToken
                });
                _logger.LogInformation($"Session Started");

                return Unit.Value;
            }
       }
    }
}
