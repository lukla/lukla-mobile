﻿using System;

namespace Lukla.Mobile.Services.Mediator.Authentication
{
    public interface IAuthenticationSettings
    {
        Uri AuthorizeEndpoint { get; set; }
        Uri CallbackUrl { get; set; }
        Uri TokenEndpoint { get; set; }
        Uri RevokeEndpoint { get; set; }
        string ClientId { get; set; }
        string ClientSecret { get; set; }
        string Scope { get; set; }
    }
}
