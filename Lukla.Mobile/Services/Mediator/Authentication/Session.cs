﻿namespace Lukla.Mobile.Services.Mediator.Authentication
{
    public class Session : ISession
    {
        public string AccessToken { get; set; }
    }
}
