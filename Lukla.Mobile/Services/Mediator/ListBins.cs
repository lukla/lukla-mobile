﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Lukla.Apis.Core.v1;
using Lukla.Apis.Core.v1.Models;
using Lukla.Mobile.Services.Api;
using Lukla.Mobile.Services.Mediator.Behaviors;
using MediatR;

namespace Lukla.Mobile.Services.Mediator
{
    public static class ListBins
    {
        public class Request : IRequest<List<ListBinsResponse>>, 
            IMobileCachingBehavior, 
            ITokenAction,
            IResilientAction { }

        public class Handler : IRequestHandler<Request, List<ListBinsResponse>>
        {
            private readonly IMobileApiFactory _apiFactory;
            private readonly IApiDirectory _apiDirectory;
            private readonly ISession _session;

            public Handler(
                IMobileApiFactory apiFactory, 
                IApiDirectory apiDirectory, 
                ISession session)
            {
                _apiFactory = apiFactory;
                _apiDirectory = apiDirectory;
                _session = session;
            }

            public Task<List<ListBinsResponse>> Handle(Request request, CancellationToken cancellationToken)
            {
                var api = _apiFactory.Create<IBinApi>(_apiDirectory.BinApiBaseUrl, _session.AccessToken);
                return api.ListBins();
            }
        }
    }
}
