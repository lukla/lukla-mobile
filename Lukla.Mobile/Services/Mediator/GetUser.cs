﻿using System.Threading;
using System.Threading.Tasks;
using Lukla.Mobile.Models;
using Lukla.Mobile.Services.Mediator.Behaviors;
using MediatR;

namespace Lukla.Mobile.Services.Mediator
{
    public static class GetUser
    {
        public class Request : IRequest<UserInfo>, IResilientAction {}

        public class Handler : IRequestHandler<Request, UserInfo>
        {
            public async Task<UserInfo> Handle(Request request, CancellationToken cancellationToken)
            {
                return new UserInfo();
            }
        }
    }
}
