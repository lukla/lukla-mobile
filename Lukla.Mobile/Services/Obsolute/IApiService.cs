﻿using System;
using Fusillade;

namespace Lukla.Mobile.Services.Obsolute
{
    [Obsolete]
    public interface IApiService<T>
    {
        T GetApi(Priority priority);
        T PostApi(Priority priority);
    }
}

