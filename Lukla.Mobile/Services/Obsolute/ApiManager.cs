﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Fusillade;
using Lukla.Mobile.Models;
using Lukla.Mobile.Services.Identity;
using Lukla.Mobile.Services.Settings;
using Plugin.Connectivity;
using Plugin.Connectivity.Abstractions;
using Polly;
using Refit;

namespace Lukla.Mobile.Services.Obsolute
{
    [Obsolete]
    public class ApiManager : IApiManager
    {
        private readonly IUserDialogs _userDialogs = UserDialogs.Instance;
        private readonly IConnectivity _connectivity = CrossConnectivity.Current;
        private readonly ISettingsService _settingsService;
        private readonly IIdentityService _identityService;
        IApiService<IAppApi> appApi;
        IApiService<IIndexApi> indexApi;
        IApiService<IUploadApi> uploadApi;
        public bool IsConnected { get; set; }
        public bool IsReachable { get; set; }

        Dictionary<int, CancellationTokenSource> runningTasks = new Dictionary<int, CancellationTokenSource>();

        public ApiManager(ISettingsService settingsService, IIdentityService identityService)
        {
            appApi = new ApiService<IAppApi>(Config.ApiUrl);
            indexApi = new ApiService<IIndexApi>(Config.ApiIndexUrl);
            uploadApi = new ApiService<IUploadApi>(Config.ApiUrl);
            IsConnected = _connectivity.IsConnected;
            _connectivity.ConnectivityChanged += OnConnectivityChanged;
            _settingsService = settingsService;
            _identityService = identityService;
        }

        void OnConnectivityChanged(object sender, ConnectivityChangedEventArgs e)
        {
            IsConnected = e.IsConnected;

            if (!e.IsConnected)
            {
                // Cancel All Running Task
                var items = runningTasks.ToList();
                foreach (var item in items)
                {
                    item.Value.Cancel();
                    runningTasks.Remove(item.Key);
                }
            }
        }

        protected async Task<LLApiResponse<T>> RemoteRequestAsync<T>(Task<ApiResponse<T>> task)
        {
            LLApiResponse<T> data = new LLApiResponse<T>();

            if (!IsConnected)
            {
                var strngResponse = "There's not a network connection";
                data.StatusCode = HttpStatusCode.BadRequest;
                data.Data = default;

                _userDialogs.Toast(strngResponse, TimeSpan.FromSeconds(1));
                return data;
            }

            IsReachable = await _connectivity.IsRemoteReachable(Config.ApiHostName);

            if (!IsReachable)
            {
                var strngResponse = "There's not an internet connection";
                data.StatusCode = HttpStatusCode.BadRequest;
                data.Data = default;

                _userDialogs.Toast(strngResponse, TimeSpan.FromSeconds(1));
                return data;
            }

            data = await Policy
            .Handle<WebException>()
            .Or<ApiException>()
            .Or<TaskCanceledException>()
            .WaitAndRetryAsync
            (
                retryCount: 1,
                sleepDurationProvider: retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt))
            )
            .ExecuteAsync(async () =>
            {
                var result = await task;
                Console.WriteLine("{0} {1}", result.RequestMessage.RequestUri.AbsoluteUri, result.StatusCode);
                if (result.StatusCode == HttpStatusCode.Unauthorized)
                {
                    Console.WriteLine("Request new token...");
                    // Refresh token
                    var refreshToken = _settingsService.RefreshToken;
                    if (!string.IsNullOrEmpty(refreshToken))
                    {
                        UserToken token = await _identityService.RefreshTokenAsync(refreshToken);
                        Console.WriteLine("Save new token {0}", token.AccessToken);
                        _settingsService.AuthAccessToken = token.AccessToken;
                        _settingsService.RefreshToken = token.RefreshToken;
                        _settingsService.AuthIdToken = token.IdToken;
                        // retry latter
                        return new LLApiResponse<T>
                        {
                            StatusCode = HttpStatusCode.Unauthorized
                        };
                    }
                }
                runningTasks.Remove(task.Id);
                var resp = new LLApiResponse<T>
                {
                    Data = result.Content,
                };
                if (result.Error != null)
                {
                    Console.WriteLine(result.Error.Content);
                    resp.ErrorMessage = result.Error.Content;
                }
                return resp;
            });

            return data;
        }

        public async Task<LLApiResponse<IListResponse<LLBin>>> GetBins()
        {
            var cts = new CancellationTokenSource();
            var task = RemoteRequestAsync(appApi.GetApi(Priority.UserInitiated).GetBins<IListResponse<LLBin>>());
            runningTasks.Add(task.Id, cts);
            var response = await task;
            if (response.StatusCode == HttpStatusCode.Unauthorized)
            {
                // Retry
                var cts1 = new CancellationTokenSource();
                var task1 = RemoteRequestAsync(appApi.GetApi(Priority.UserInitiated).GetBins<IListResponse<LLBin>>());
                runningTasks.Add(task1.Id, cts1);
                response = await task1;
            }
            return response;
        }

        public async Task<LLApiResponse<IListResponse<LLCompany>>> GetCompanies()
        {
            var cts = new CancellationTokenSource();
            var task = RemoteRequestAsync(appApi.GetApi(Priority.UserInitiated).GetCompanies<IListResponse<LLCompany>>());
            runningTasks.Add(task.Id, cts);
            var response = await task;
            if (response.StatusCode == HttpStatusCode.Unauthorized)
            {
                // Retry
                var cts1 = new CancellationTokenSource();
                var task1 = RemoteRequestAsync(appApi.GetApi(Priority.UserInitiated).GetCompanies<IListResponse<LLCompany>>());
                runningTasks.Add(task1.Id, cts1);
                response = await task1;
            }
            return response;
        }

        public async Task<LLApiResponse<string>> SwitchCompany(string companyId)
        {
            var cts = new CancellationTokenSource();
            var task = RemoteRequestAsync(appApi.PostApi(Priority.UserInitiated).SwitchCompany<string>(companyId));
            runningTasks.Add(task.Id, cts);
            var response = await task;
            if (response.StatusCode == HttpStatusCode.Unauthorized)
            {
                // Retry
                var cts1 = new CancellationTokenSource();
                var task1 = RemoteRequestAsync(appApi.PostApi(Priority.UserInitiated).SwitchCompany<string>(companyId));
                runningTasks.Add(task1.Id, cts1);
                response = await task1;
            }
            return response;
        }

        public async Task<LLApiResponse<IListResponse<LLDocument>>> GetDocuments(DocumentSearchParams filter)
        {
            var cts = new CancellationTokenSource();
            var task = RemoteRequestAsync(indexApi.GetApi(Priority.UserInitiated).GetDocs<IListResponse<LLDocument>>(filter));
            runningTasks.Add(task.Id, cts);
            var response = await task;
            if (response.StatusCode == HttpStatusCode.Unauthorized)
            {
                // Retry
                var cts1 = new CancellationTokenSource();
                var task1 = RemoteRequestAsync(indexApi.GetApi(Priority.UserInitiated).GetDocs<IListResponse<LLDocument>>(filter));
                runningTasks.Add(task.Id, cts1);
                response = await task1;
            }
            return response;
        }

        public async Task<LLApiResponse<UserInfo>> GetProfile()
        {
            var cts = new CancellationTokenSource();
            var task = RemoteRequestAsync(appApi.GetApi(Priority.UserInitiated).GetProfile<UserInfo>());
            runningTasks.Add(task.Id, cts);
            var response = await task;
            if (response.StatusCode == HttpStatusCode.Unauthorized)
            {
                // Retry
                var cts1 = new CancellationTokenSource();
                var task1 = RemoteRequestAsync(appApi.GetApi(Priority.UserInitiated).GetProfile<UserInfo>());
                runningTasks.Add(task1.Id, cts1);
                response = await task1;
            }
            return response;
        }

        public async Task<LLApiResponse<Stream>> DownloadDoc(string versionId)
        {
            var cts = new CancellationTokenSource();
            var task = appApi.GetApi(Priority.UserInitiated).DownloadDoc<object>(versionId);
            runningTasks.Add(task.Id, cts);
            var response = await task;
            var result = new LLApiResponse<Stream>();
            if (response.IsSuccessStatusCode)
            {
                result.Data = await response.Content.ReadAsStreamAsync();
            } else
            {
                result.ErrorMessage = $"Error: {response.StatusCode}";
            }

            return result;
        }

        public async Task<LLApiResponse<IListResponse<SecurityCode>>> GetSecurityCodes()
        {
            var cts = new CancellationTokenSource();
            var task = RemoteRequestAsync(appApi.GetApi(Priority.UserInitiated).GetSecurityCodes<IListResponse<SecurityCode>>());
            runningTasks.Add(task.Id, cts);
            return await task;
        }

        public async Task<LLApiResponse<List<LLAnchor>>> GetAnchors()
        {
            var cts = new CancellationTokenSource();
            var task = RemoteRequestAsync(appApi.GetApi(Priority.UserInitiated).GetAnchors<List<LLAnchor>>());
            runningTasks.Add(task.Id, cts);
            return await task;
        }

        public async Task<LLApiResponse<VersionIdResponse>> UploadVersion(UploadVersionParam p)
        {
            var cts = new CancellationTokenSource();
            var task = RemoteRequestAsync(uploadApi.PostApi(Priority.UserInitiated).UploadVersion<VersionIdResponse>(
                    p.DocumentId, p.Anchors, p.FileName, new StreamPart(p.File, p.FileName)
                ));
            runningTasks.Add(task.Id, cts);
            return await task;
        }

        public async Task<LLApiResponse<LLDocument>> GetDocumentMeta(string versionId)
        {
            var cts = new CancellationTokenSource();
            var task = RemoteRequestAsync(appApi.GetApi(Priority.UserInitiated).GetDocumentMeta<LLDocument>(versionId));
            runningTasks.Add(task.Id, cts);
            return await task;
        }

        public async Task<LLApiResponse<string>> UpdateDocument(string p)
        {
            var cts = new CancellationTokenSource();
            var task = RemoteRequestAsync(appApi.GetApi(Priority.UserInitiated).UpdateDocument<string>(p));
            runningTasks.Add(task.Id, cts);
            return await task;
        }

        public async Task<LLApiResponse<VersionIdResponse>> CloneDocument(UploadDocParams p)
        {
            var cts = new CancellationTokenSource();
            var stream = new StreamPart(p.File, p.FileName);
            var task = RemoteRequestAsync(uploadApi.GetApi(Priority.UserInitiated).CloneDocument<VersionIdResponse>(p.RequestJson, stream));
            runningTasks.Add(task.Id, cts);
            return await task;
        }
    }
}
