using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Lukla.Mobile.Models;
using Xamarin.Forms;

namespace Lukla.Mobile.Services.Settings
{
    public class SettingsService : ISettingsService
    {
        #region Setting Constants

        private const string AccessToken = "access_token";
        private const string IdToken = "id_token";
        private const string AuthCodeKey = "auth_code";
        private const string RefreshTokenKey = "refresh_token";
        private readonly string AccessTokenDefault = string.Empty;
        private readonly string IdTokenDefault = string.Empty;
        #endregion
        
        #region Settings Properties

        public string AuthAccessToken
        {
            get => GetValueOrDefault(AccessToken, AccessTokenDefault);
            set => AddOrUpdateValue(AccessToken, value);
        }

        public string AuthIdToken
        {
            get => GetValueOrDefault(IdToken, IdTokenDefault);
            set => AddOrUpdateValue(IdToken, value);
        }

        public string AuthCode
        {
            get => GetValueOrDefault(AuthCodeKey, string.Empty);
            set => AddOrUpdateValue(AuthCodeKey, value);
        }

        public string RefreshToken
        {
            get => GetValueOrDefault(RefreshTokenKey, string.Empty);
            set => AddOrUpdateValue(RefreshTokenKey, value);
        }

        public bool NeedRefeshBins { get; set; }

        public UserInfo UserInfo { get; set; }

        public void ClearTokens()
        {
            AuthAccessToken = string.Empty;
            AuthCode = string.Empty;
            AuthIdToken = string.Empty;
            RefreshToken = string.Empty;
            Debug.WriteLine("ClearTokens SUCCESS");
        }

        #endregion

        #region Public Methods

        public Task AddOrUpdateValue(string key, string value) => AddOrUpdateValueInternal(key, value);
        public string GetValueOrDefault(string key, string defaultValue) => GetValueOrDefaultInternal(key, defaultValue);

        #endregion

        #region Internal Implementation

        async Task AddOrUpdateValueInternal<T>(string key, T value)
        {
            if (value == null)
            {
                await Remove(key);
            }

            Application.Current.Properties[key] = value;
            try
            {
                await Application.Current.SavePropertiesAsync();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Unable to save: " + key, " Message: " + ex.Message);
            }
        }

        T GetValueOrDefaultInternal<T>(string key, T defaultValue = default(T))
        {
            object value = null;
            if (Application.Current.Properties.ContainsKey(key))
            {
                value = Application.Current.Properties[key];
            }
            return null != value ? (T)value : defaultValue;
        }

        async Task Remove(string key)
        {
            try
            {
                if (Application.Current.Properties[key] != null)
                {
                    Application.Current.Properties.Remove(key);
                    await Application.Current.SavePropertiesAsync();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Unable to remove: " + key, " Message: " + ex.Message);
            }
        }

        #endregion
    }
}