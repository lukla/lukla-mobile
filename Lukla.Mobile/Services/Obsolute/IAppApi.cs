﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Refit;

namespace Lukla.Mobile.Services.Obsolute
{
    [Obsolete]
    [Headers("Content-Type: application/json")]
    public interface IAppApi
    {
        [Get("/bins/list")]
        Task<ApiResponse<T>> GetBins<T>();

        [Get("/profile/companies")]
        Task<ApiResponse<T>> GetCompanies<T>();

        [Post("/switch/companies/{companyId}")]
        Task<ApiResponse<T>> SwitchCompany<T>(string companyId);

        [Get("/profile")]
        Task<ApiResponse<T>> GetProfile<T>();

        [Get("/content/{versionId}")]
        Task<HttpResponseMessage> DownloadDoc<T>(string versionId);

        [Get("/options/security-codes")]
        Task<ApiResponse<T>> GetSecurityCodes<T>();

        [Get("/anchors")]
        Task<ApiResponse<T>> GetAnchors<T>();

        [Get("/meta/{versionId}")]
        Task<ApiResponse<T>> GetDocumentMeta<T>(string versionId);

        [Post("/document/meta")]
        Task<ApiResponse<T>> UpdateDocument<T>([Body]string p);
    }

    public interface IUploadApi
    {
        [Multipart]
        [Post("/content/version")]
        Task<ApiResponse<T>> UploadVersion<T>(string DocumentId, string Anchors, string FileName, StreamPart file);

        [Multipart]
        [Post("/content")]
        Task<ApiResponse<T>> CloneDocument<T>(string request, StreamPart file);
    }
}
