﻿using System;
using System.Threading.Tasks;
using Lukla.Mobile.Models;
using Refit;

namespace Lukla.Mobile.Services.Obsolute
{
    [Obsolete]
    [Headers("Content-Type: application/json")]
    public interface IIndexApi
    {
        [Get("/index/search")]
        Task<ApiResponse<T>> GetDocs<T>(DocumentSearchParams p);
    }
}
