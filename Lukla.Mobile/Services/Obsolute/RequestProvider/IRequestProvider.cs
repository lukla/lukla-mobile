﻿using System.Threading.Tasks;

namespace Lukla.Mobile.Services.RequestProvider
{
    public interface IRequestProvider
    {
        Task<TResult> PostAsync<TResult>(string uri, string data, string clientId, string clientSecret);
    }
}