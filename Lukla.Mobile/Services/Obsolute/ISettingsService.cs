﻿using Lukla.Mobile.Models;

namespace Lukla.Mobile.Services.Settings
{
    public interface ISettingsService
    {
        string AuthAccessToken { get; set; }
        string AuthIdToken { get; set; }
        string AuthCode { get; set; }
        string RefreshToken { get; set; }

        //  TODO: Why Need this?
        bool NeedRefeshBins { get; set; }
        UserInfo UserInfo { get; set; }

        //  TODO: Put in different service
        void ClearTokens();
    }
}
