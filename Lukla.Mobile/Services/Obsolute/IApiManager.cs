﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Lukla.Mobile.Models;

namespace Lukla.Mobile.Services.Obsolute
{
    //  TODO: Convert these to mediator commands/requests

    [Obsolete]
    public interface IApiManager
    {
        Task<LLApiResponse<IListResponse<LLBin>>> GetBins();
        Task<LLApiResponse<IListResponse<LLCompany>>> GetCompanies();
        Task<LLApiResponse<string>> SwitchCompany(string companyId);
        Task<LLApiResponse<IListResponse<LLDocument>>> GetDocuments(DocumentSearchParams filter);
        Task<LLApiResponse<UserInfo>> GetProfile();
        Task<LLApiResponse<Stream>> DownloadDoc(string versionId);
        Task<LLApiResponse<IListResponse<SecurityCode>>> GetSecurityCodes();
        Task<LLApiResponse<List<LLAnchor>>> GetAnchors();
        Task<LLApiResponse<VersionIdResponse>> UploadVersion(UploadVersionParam version);
        Task<LLApiResponse<LLDocument>> GetDocumentMeta(string versionId);
        Task<LLApiResponse<string>> UpdateDocument(string p);
        Task<LLApiResponse<VersionIdResponse>> CloneDocument(UploadDocParams p);
    }
}
