﻿using System;
using System.Collections.Generic;
using System.Text;
using DryIoc;
using Microsoft.Extensions.Logging;

namespace Lukla.Mobile.Services.Startup
{
    public static class ConfigureLogger
    {
        public static IContainer RegisterLogger(this IContainer container)
        {
            // create logger factory
            ILoggerFactory loggerFactory = new LoggerFactory();

            // register factory
            container.UseInstance(loggerFactory);

            // get the factory method
            var loggerFactoryMethod = typeof(LoggerFactoryExtensions).GetMethod("CreateLogger", new Type[] { typeof(ILoggerFactory) });

            // register logger with factory method
            container.Register(typeof(ILogger<>), made: Made.Of(req => loggerFactoryMethod.MakeGenericMethod(req.Parent.ImplementationType)));

            return container;
        }
    }
}
