﻿using System.Linq;
using DryIoc;
using Lukla.Mobile.Services.Mediator.Behaviors;
using MediatR;

namespace Lukla.Mobile.Services.Mediator
{
    public static class ConfigureMediator
    {
        public static IContainer RegisterMediator(this IContainer container)
        {
            container.RegisterDelegate<ServiceFactory>(r => r.Resolve);
            container.Register<IMediator, MediatR.Mediator>();
            container.RegisterMany(typeof(TestCommand.Handler).GetAssembly().GetTypes().Where(t => t.IsMediatorHandler()));
            container.Register(typeof(IPipelineBehavior<,>), typeof(ResilientBehavior<,>));
            container.Register(typeof(IPipelineBehavior<,>), typeof(MobileCacheBehavior<,>));
            container.Register(typeof(IPipelineBehavior<,>), typeof(TokenBehavior<,>));

            return container;
        }
    }
}
