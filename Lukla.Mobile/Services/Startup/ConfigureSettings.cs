﻿using System;
using DryIoc;
using Lukla.Mobile.Services.Mediator.Authentication;

namespace Lukla.Mobile.Services.Settings
{
    public static class ConfigureSettings
    {
        public static IContainer RegisterSettings(this IContainer container)
        {
            return container.ConfigureDev();
        }

        private static IContainer ConfigureDev(this IContainer container)
        {
            var endpoint = "https://auth-beta.lukla.com";
            var authenticationSettings = new AuthenticationSettings
            {
                AuthorizeEndpoint = new Uri($"{endpoint}/connect/authorize"),
                TokenEndpoint = new Uri($"{endpoint}/connect/token"),
                CallbackUrl = new Uri("lukla.mobile://auth/"),
                ClientId = "lukla_mobile_client",
                ClientSecret = "9560fd57-28dc-4ac9-a12d-c0f3900ac036",
                Scope = "lukla_document_api lukla_index_api offline_access openid profile"
            };

            container.RegisterInstance<IAuthenticationSettings>(authenticationSettings);
            return container;
        }
    }
}
