﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;

namespace Lukla.Mobile.Services
{
    public static class TestCommand
    {
        public class Command : IRequest
        {

        }

        public class Handler : IRequestHandler<Command>
        {
            public Task<Unit> Handle(Command request, CancellationToken cancellationToken)
            {
                Console.Write("Hi");
                return Unit.Task;
            }
        }
    }
}
