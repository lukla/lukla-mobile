﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using IdentityModel;
using Lukla.Mobile.Helpers;
using Lukla.Mobile.Models;
using Lukla.Mobile.Services.RequestProvider;
using Xamarin.Forms;

namespace Lukla.Mobile.Services.Identity
{
    public class IdentityService : IIdentityService
    {
        //  TODO: Do this need to be stored as state variable?
        private string _codeVerifier;
        public string CodeVerifier
        {
            get
            {
                if (!string.IsNullOrEmpty(_codeVerifier)) return _codeVerifier;

                if (Application.Current.Properties.ContainsKey("code_verifier"))
                {
                    return (string)Application.Current.Properties["code_verifier"];
                }

                return string.Empty;
            }
            set
            {
                _codeVerifier = value;
                Application.Current.Properties["code_verifier"] = value;
            }
        }

        private readonly IRequestProvider _requestProvider;

        public IdentityService(IRequestProvider requestProvider)
        {
            _requestProvider = requestProvider;
        }

        public string CreateAuthorizationRequest()
        {
            // Create URI to authorization endpoint
            var authorizeRequest = new AuthorizeRequest(GlobalSetting.Instance.AuthorizeEndpoint);

            // Dictionary with values for the authorize request
            var currentCSRFToken = Guid.NewGuid().ToString("N");
            var dic = new Dictionary<string, string>
            {
                {"client_id", GlobalSetting.Instance.ClientId},
                {"client_secret", GlobalSetting.Instance.ClientSecret},
                {"response_type", "code"},
                {"access_type", "offline"},
                {"scope", GlobalSetting.Instance.Scope},
                {"redirect_uri", GlobalSetting.Instance.Callback},
                {"nonce", Guid.NewGuid().ToString("N")},
                {"code_challenge", CreateCodeChallenge()},
                {"code_challenge_method", "S256"},
                {"grant_type", "authorization_code"},
                {"state", currentCSRFToken}
            };

            var authorizeUri = authorizeRequest.Create(dic);
            return authorizeUri;
        }

        public string CreateLogoutRequest(string token)
        {
            return string.IsNullOrEmpty(token) ? string.Empty : 
                $"{GlobalSetting.Instance.LogoutEndpoint}?id_token_hint={token}&post_logout_redirect_uri={GlobalSetting.Instance.Callback}";
        }

        public async Task<UserToken> GetTokenAsync(string code)
        {
            var data =
                $"grant_type=authorization_code&code={code}&redirect_uri={WebUtility.UrlEncode(GlobalSetting.Instance.Callback)}&code_verifier={CodeVerifier}&client_id={GlobalSetting.Instance.ClientId}&client_secret={GlobalSetting.Instance.ClientSecret}";
            
            var token = await _requestProvider.PostAsync<UserToken>(GlobalSetting.Instance.TokenEndpoint, data, GlobalSetting.Instance.ClientId, GlobalSetting.Instance.ClientSecret);
            return token;
        }

        public async Task<UserToken> RefreshTokenAsync(string refreshToken)
        {
            var data =
                $"grant_type=refresh_token&refresh_token={refreshToken}&redirect_uri={WebUtility.UrlEncode(GlobalSetting.Instance.Callback)}&code_verifier={CodeVerifier}&client_id={GlobalSetting.Instance.ClientId}&client_secret={GlobalSetting.Instance.ClientSecret}";
            
            var token = await _requestProvider.PostAsync<UserToken>(GlobalSetting.Instance.TokenEndpoint, data, GlobalSetting.Instance.ClientId, GlobalSetting.Instance.ClientSecret);
            return token;
        }

        private string CreateCodeChallenge()
        {
            CodeVerifier = Utils.RandomString(128);
            var sHA256 = SHA256.Create();
            var challengeBytes = sHA256.ComputeHash(Encoding.UTF8.GetBytes(CodeVerifier));
            var code = Base64Url.Encode(challengeBytes);
            return code;
        }
    }
}
