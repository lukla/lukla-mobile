﻿using System.Threading.Tasks;
using Lukla.Mobile.Models;

namespace Lukla.Mobile.Services.Identity
{
    public interface IIdentityService
    {
        string CreateAuthorizationRequest();
        string CreateLogoutRequest(string token);
        Task<UserToken> GetTokenAsync(string code);
        Task<UserToken> RefreshTokenAsync(string refreshToken);
    }
}