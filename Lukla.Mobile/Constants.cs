﻿namespace Lukla.Mobile
{
    public static class Constants
    {
        public static int DocPageSize = 10;
        public static string UrlHomePage = "https://lukla.co/";
        public static string UrlTerms = "https://lukla.co/terms-conditions";
        public static string UrlPrivacy = "https://lukla.co/terms-conditions";
    }
}
