﻿using System.Text.RegularExpressions;

namespace Lukla.Mobile
{
    public static class Config
    {
        public static string ApiUrl = "https://documents-beta.lukla.com";
        public static string ApiIndexUrl = "https://index-beta.lukla.com";
        public static string ApiHostName
        {
            get
            {
                var apiHostName = Regex.Replace(ApiUrl, @"^(?:http(?:s)?://)?(?:www(?:[0-9]+)?\.)?", string.Empty, RegexOptions.IgnoreCase)
                                   .Replace("/", string.Empty);
                return apiHostName;
            }
        }
    }
}
