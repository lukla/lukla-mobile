﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Lukla.Mobile.Models;
using Lukla.Mobile.Services.Identity;
using Lukla.Mobile.Services.Obsolute;
using Lukla.Mobile.Services.Settings;
using Prism.Commands;
using Prism.Navigation;

namespace Lukla.Mobile.ViewModels
{
    public class SwitchCompanyViewModel : ViewModelBase
    {
        private ISettingsService _settingsService;
        private IIdentityService _identityService;
        public DelegateCommand SelectionDone { get; private set; }
        List<LLCompany> _companies;
        public List<LLCompany> Companies
        {
            get => _companies;
            set
            {
                SetProperty(ref _companies, value, () =>
                {
                    // set selected company
                    if (_companies.Count > 0 && _settingsService.UserInfo != null)
                    {
                        SelectedCompany = null;
                        SelectedCompany = _companies.Find(x => x.Id == _settingsService.UserInfo.CompanyId);
                    }
                });
            }
        }
        public LLCompany PreviousCompany { get; private set; }
        LLCompany _selectedCompany;
        public LLCompany SelectedCompany
        {
            get => _selectedCompany;
            set
            {
                if (_selectedCompany != value)
                {
                    Console.WriteLine("SelectedDocument");
                    SetProperty(ref _selectedCompany, value, () =>
                    {
                        TickAndUntickSelectedItem();
                    });
                }
            }
        }

        public SwitchCompanyViewModel(
        ISettingsService settingsService,
        IIdentityService identityService,
        IApiManager _apiManager,
        INavigationService navigationService) : base(navigationService, _apiManager)
        {
            Title = "Switch Company";
            _identityService = identityService;
            _settingsService = settingsService;
            SelectionDone = new DelegateCommand(async () => {
                if (_settingsService.UserInfo.CompanyId != SelectedCompany.Id)
                {
                    await SubmitSelection();
                } else
                {
                    await NavigationService.GoBackAsync();
                }
            });
            Companies = new List<LLCompany>();
            _ = RunSafe(FetchCompanies());
        }

        private void TickAndUntickSelectedItem()
        {
            if (PreviousCompany != null)
            {
                Companies.Where(t => t.Id == PreviousCompany.Id).FirstOrDefault().IsSelected = false;
            }
            Companies.Where(t => t.Id == SelectedCompany.Id).FirstOrDefault().IsSelected = true;
            PreviousCompany = SelectedCompany;
        }

        private async Task SubmitSelection()
        {
            var success = await RunSafeWithResult(RequestSwitchCompany());
            if (success)
            {
                var updateTokenSuccess = await RequestNewToken();
                if (updateTokenSuccess)
                {
                    _settingsService.NeedRefeshBins = true;
                    var navigationParams = new NavigationParameters();
                    navigationParams.Add("SwitchCompanySuccess", true);
                    await NavigationService.GoBackAsync(navigationParams);
                }
            }
        }

        private async Task<string> RequestSwitchCompany()
        {
            var companyId = SelectedCompany.Id;
            var result = await ApiManager.SwitchCompany(companyId);
            return string.IsNullOrEmpty(result.ErrorMessage) ? null : result.ErrorMessage;
        }

        private async Task<bool> RequestNewToken()
        {
            var refreshToken = _settingsService.RefreshToken;
            if (!string.IsNullOrEmpty(refreshToken))
            {
                var userToken = await _identityService.RefreshTokenAsync(refreshToken);
                string accessToken = userToken.AccessToken;

                if (!string.IsNullOrWhiteSpace(accessToken))
                {
                    _settingsService.AuthAccessToken = accessToken;
                    _settingsService.AuthIdToken = userToken.IdToken;
                    _settingsService.RefreshToken = userToken.RefreshToken;
                    return true;
                } else
                {
                    return false;
                }
            }
            return false;
        }

        private async Task<string> FetchCompanies()
        {
            var companiesResponse = await ApiManager.GetCompanies();
            if (companiesResponse.Data != null)
            {
                var companies = companiesResponse.Data;
                Companies = companies.Items;
                Console.WriteLine("Loaded companies: {0} companies", Companies.Count);
            }
            else
            {
                return companiesResponse.ErrorMessage ?? Strings.GeneralError;
            }
            return null;
        }
    }
}