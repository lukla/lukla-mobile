﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Lukla.Mobile.Services;
using Lukla.Mobile.Services.Mediator.Authentication;
using MediatR;
using Prism.Mvvm;
using Prism.Navigation;
using Xamarin.Forms;

namespace Lukla.Mobile.ViewModels
{
    public class TestViewModel : BindableBase, IInitialize

    {
        private readonly IMediator _mediator;
        public TestViewModel(IMediator mediator)
        {
            _mediator = mediator;
            AuthCommand = new Command(async () => await _mediator.Send(new StartSession.Command()));

        }
        public ICommand AuthCommand { get; }

        public async void Initialize(INavigationParameters parameters)
        {
            
        }
    }
}
