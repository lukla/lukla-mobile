﻿using System;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Lukla.Mobile.Models;
using Lukla.Mobile.Services;
using Lukla.Mobile.Services.Obsolute;
using Lukla.Mobile.Services.Settings;
using MediatR;
using Prism.Commands;
using Prism.Navigation;

namespace Lukla.Mobile.ViewModels
{
    public class SettingViewModel : ViewModelBase
    {
        public DelegateCommand Logout { get; private set; }
        public DelegateCommand SwitchCompany { get; private set; }
        public DelegateCommand GotoHomePage { get; private set; }
        public DelegateCommand GotoTerms { get; private set; }
        public DelegateCommand GotoPolicy { get; private set; }

        private ISettingsService _settingService;
        private UserInfo _user;
        public UserInfo User
        {
            get => _user;
            set => SetProperty(ref _user, value);
        }


        public SettingViewModel(
            INavigationService navigationService,
            IApiManager _apiManager, 
            ISettingsService settingService,
            IMediator _mediator
            )
           : base(navigationService, _apiManager)
        {
            Title = "Setting";
            _settingService = settingService;
            User = _settingService.UserInfo;

            if (_settingService.UserInfo == null)
            {
                // load user if previous request failed
                _ = GetUserInfo();
            }

            Logout = new DelegateCommand(async () =>
                {
                    var result = await UserDialogs.Instance.ConfirmAsync(new ConfirmConfig
                    {
                        Message = "Are you sure you want to logout?",
                        OkText = "OK",
                        CancelText = "Cancel"
                    });
                    if (result)
                    {
                        // logout
                        await NavigationService.NavigateAsync("Logout");
                    }
                }
            );
            SwitchCompany = new DelegateCommand(async () =>
            {
                await NavigationService.NavigateAsync("SwitchCompany");
            });
            GotoHomePage = new DelegateCommand(async () =>
            {
                await _mediator.Send(new TestCommand.Command());
                await OpenBrowser(Constants.UrlHomePage);
            });
            GotoTerms = new DelegateCommand(async () =>
            {
                await OpenBrowser(Constants.UrlTerms);
            });
            GotoPolicy = new DelegateCommand(async () =>
            {
                await OpenBrowser(Constants.UrlPrivacy);
            });
        }

        private async Task GetUserInfo()
        {
            var response = await ApiManager.GetProfile();
            if (response.Data != null)
            {
                User = response.Data;
                _settingService.UserInfo = User;
            }
        }

        override public void OnNavigatedTo(INavigationParameters parameters)
        {
            if (parameters.ContainsKey("SwitchCompanySuccess"))
            {
                _ = GetUserInfo();
            }
        }
        override public void OnNavigatedFrom(INavigationParameters parameters)
        {
            Console.WriteLine(parameters);
        }
    }
}
