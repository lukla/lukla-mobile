﻿using System.Threading.Tasks;
using Acr.UserDialogs;
using Lukla.Mobile.Services.Obsolute;
using Prism.Mvvm;
using Prism.Navigation;
using Xamarin.Essentials;

namespace Lukla.Mobile.ViewModels
{
    public struct PickerSelection
    {
        public PickerSelection(string title, int id)
        {
            Title = title;
            Id = id;
        }

        public string Title { get; }
        public int Id { get; }
    }

    public class ViewModelBase : BindableBase, IInitialize, INavigationAware, IDestructible
    {
        protected INavigationService NavigationService { get; private set; }
        protected IUserDialogs PageDialog = UserDialogs.Instance;

        public IApiManager ApiManager;

        IApiService<IAppApi> appApi = new ApiService<IAppApi>(Config.ApiUrl);

        private string _title;
        private bool isBusy;

        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        public bool SelfHandleLoading { get; set; }

        public bool IsBusy
        {
            get => isBusy;
            set
            {
                var hasChanged = isBusy != value;
                SetProperty(ref isBusy, value, () =>
                {
                    if (!SelfHandleLoading && hasChanged)
                    {
                        if (isBusy)
                        {
                            UserDialogs.Instance.ShowLoading("");
                        } else
                        {
                            UserDialogs.Instance.HideLoading();
                        }
                    }
                });
            }
        }

        public ViewModelBase(INavigationService navigationService, IApiManager _apiManager)
        {
            NavigationService = navigationService;
            ApiManager = _apiManager;
        }

        public virtual void Initialize(INavigationParameters parameters)
        {

        }

        public virtual void OnNavigatedFrom(INavigationParameters parameters)
        {

        }

        public virtual void OnNavigatedTo(INavigationParameters parameters)
        {

        }

        public virtual void Destroy()
        {

        }

        public async Task RunSafe(Task<string> task, bool ShowLoading = true, string loadingMessage = null)
        {
            string errorMessage = null;
            try
            {
                if (IsBusy) return;

                IsBusy = true;
                errorMessage = await task;
            }
            catch (System.Exception e)
            {
                IsBusy = false;
                await App.Current.MainPage.DisplayAlert("Error", e.Message, "Ok");
            }
            finally
            {
                IsBusy = false;
                if (errorMessage != null)
                {
                    await App.Current.MainPage.DisplayAlert("Error", errorMessage, "Ok");
                }
            }
        }

        public async Task<bool> RunSafeWithResult(Task<string> task, bool ShowLoading = true, string loadingMessage = null)
        {
            string errorMessage = null;
            try
            {
                if (IsBusy) return false;

                IsBusy = true;
                errorMessage = await task;
                return errorMessage == null;
            }
            catch (System.Exception e)
            {
                IsBusy = false;
                await App.Current.MainPage.DisplayAlert("Error", e.Message, "Ok");
                return false;
            }
            finally
            {
                IsBusy = false;
                if (errorMessage != null)
                {
                    await App.Current.MainPage.DisplayAlert("Error", errorMessage, "Ok");
                }
            }
        }

        public async Task OpenBrowser(string url)
        {
            var uri = new System.Uri(url);
            await Browser.OpenAsync(uri, BrowserLaunchMode.SystemPreferred);
        }
    }
}
