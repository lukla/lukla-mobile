﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Lukla.Mobile.Helpers;
using Lukla.Mobile.Models;
using Lukla.Mobile.Services.Obsolute;
using Newtonsoft.Json;
using Prism.Commands;
using Prism.Navigation;

namespace Lukla.Mobile.ViewModels
{
    public class EditDocInfoViewModel : ViewModelBase
    {
        private IKeyboardHelper _keyboardHelper;
        public DelegateCommand Save { get; private set; }
        private string _binId { get; set; }
        private LLAnchor[] _templateAnchors { get; set; }
        private LLDocument _docMeta;
        public LLDocument DocMeta
        {
            get => _docMeta;
            set
            {
                // pre-process anchors here
                SetProperty(ref _docMeta, value, () =>
                {
                    if (_templateAnchors != null)
                    {
                        if (_templateAnchors != null)
                        {
                            Anchors = _docMeta.TagsToFilledValueAnchors(_templateAnchors);
                        }
                    }
                    if (SecurityCodes != null && SecurityCodes.Count > 0)
                    {
                        SelectedSecurity = SecurityCodes.Find(e => e.Rank == DocMeta.SecurityRank);
                    }
                });
            }
        }
        
        private List<SecurityCode> _securityCodes;
        public List<SecurityCode> SecurityCodes
        {
            get => _securityCodes;
            set
            {
                SetProperty(ref _securityCodes, value, () =>
                {
                    if (DocMeta != null)
                    {
                        SelectedSecurity = SecurityCodes.Find(e => e.Rank == DocMeta.SecurityRank);
                    }
                });
            }
        }

        private List<LLAnchor> _anchors;
        public List<LLAnchor> Anchors
        {
            get => _anchors;
            set
            {
                SetProperty(ref _anchors, value, () =>
                {
                    HeightAnchorList = _anchors.Count * 70;
                });
            }
        }

        private SecurityCode _selectedSecurity;
        public SecurityCode SelectedSecurity
        {
            get => _selectedSecurity;
            set
            {
                SetProperty(ref _selectedSecurity, value);
            }
        }

        private UploadVersionParam _apiParams;
        public UploadVersionParam ApiParams
        {
            get => _apiParams;
            set
            {
                SetProperty(ref _apiParams, value);
            }
        }

        private int _heightAnchorList;
        public int HeightAnchorList { get => _heightAnchorList; set { SetProperty(ref _heightAnchorList, value); } }

        public EditDocInfoViewModel(INavigationService navigationService, IApiManager _apiManager, IKeyboardHelper keyboardHelper)
            : base(navigationService, _apiManager)
        {
            _keyboardHelper = keyboardHelper;
            Title = "Edit Info";
            SecurityCodes = new List<SecurityCode>();
            Save = new DelegateCommand(() =>
            {
                _ = SaveDocAsync();
            });
            InitialRequest();
        }

        private async void InitialRequest()
        {
            await GetSecurityCodes();
            await GetAnchors();
        }

        public async Task<string> GetSecurityCodes()
        {
            var resp = await ApiManager.GetSecurityCodes();

            if (resp.Data != null)
            {
                var codes = resp.Data;
                SecurityCodes = codes.Items;
                SelectedSecurity = SecurityCodes.First();
            }
            return resp.ErrorMessage;
        }


        public async Task<string> GetDocumentMeta(string versionId)
        {
            var resp = await ApiManager.GetDocumentMeta(versionId);

            if (resp.Data != null)
            {
                DocMeta = resp.Data;
            }
            return resp.ErrorMessage;
        }

        public async Task<string> GetAnchors()
        {
            var resp = await ApiManager.GetAnchors();

            if (resp.Data != null)
            {
                _templateAnchors = ProcessAnchors(resp.Data);
                if (_docMeta != null)
                {
                    Anchors = _docMeta.TagsToFilledValueAnchors(_templateAnchors);
                }
            }
            return resp.ErrorMessage;
        }

        private LLAnchor[] ProcessAnchors(List<LLAnchor> anchors)
        {
            // order and filter anchors isEdited = true
            anchors = anchors.OrderBy(e => e.DisplayOrder).ToList();
            anchors = anchors.FindAll(e => e.IsEditable == true);
            return anchors.ToArray();
        }

        public async Task SaveDocAsync()
        {
            _keyboardHelper.HideKeyboard();
            if (!ValidateInputs())
            {
                await App.Current.MainPage.DisplayAlert("Edit", "Please enter valid values for required fields", "OK");
                return;
            }
            var anchorsJson = Utils.ToArrayAnchorDict(Anchors);
            // create parameters
            var editParams = new EditDocParams
            {
                DocumentId = DocMeta.Id,
                Title = DocMeta.Title,
                SecurityCodeId = SelectedSecurity.Id,
                Anchors = anchorsJson
            };
            var json = JsonConvert.SerializeObject(editParams);
            var success = await RunSafeWithResult(RequestUpdateVersion(json));
            if (success)
            {
                await App.Current.MainPage.DisplayAlert("", "Update document success!", "Ok");
                var navigationParams = new NavigationParameters
                {
                    { "RefreshDocList", true }
                };
                await NavigationService.GoBackAsync(navigationParams);
            }
        }

        public bool ValidateInputs()
        {
            var isValid = true;

            // title
            if (!Utils.IsValidFormValue(DocMeta.Title)) { return false; }

            // security code
            if (SelectedSecurity == null) { return false; }

            // required anchors
            Anchors.ForEach(anchor =>
            {
                if (!anchor.IsValidInputValue())
                {
                    isValid = false;
                    return;
                }
            });
            return isValid;
        }

        public async Task<string> RequestUpdateVersion(string p)
        {
            var resp = await ApiManager.UpdateDocument(p);
            return resp.ErrorMessage;
        }

        override public void OnNavigatedTo(INavigationParameters parameters)
        {
            if (parameters.ContainsKey("Doc") && parameters.ContainsKey("BinId"))
            {
                var doc = (LLDocument)parameters["Doc"];
                _binId = (string)parameters["BinId"];
                _ = RunSafe(GetDocumentMeta(doc.VersionId));
            }
        }
    }
}
