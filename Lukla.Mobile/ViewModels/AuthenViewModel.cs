﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using IdentityModel.Client;
using Lukla.Mobile.Services.Identity;
using Lukla.Mobile.Services.Obsolute;
using Lukla.Mobile.Services.Settings;
using Prism.Navigation;

namespace Lukla.Mobile.ViewModels
{
    public class AuthenViewModel : ViewModelBase
    {
        //  TODO: Is this used
        private bool _isValid;
        public bool IsValid
        {
            get => _isValid;
            set => SetProperty(ref _isValid, value);
        }

        private bool _isLogin;
        public bool IsLogin
        {
            get => _isLogin;
            set => SetProperty(ref _isLogin, value);
        }

        private string _authUrl;
        public string AuthUrl
        {
            get => _authUrl;
            set => SetProperty(ref _authUrl, value);
        }

        private bool _showWebView;
        public bool ShowWebView 
        { 
            get => _showWebView; 
            set => SetProperty(ref _showWebView, value);
        }

        private readonly ISettingsService _settingsService;
        private readonly IIdentityService _identityService;

        public AuthenViewModel(
        ISettingsService settingsService,
        IIdentityService identityService,
        IApiManager apiManager,
        INavigationService navigationService)
        : base(navigationService, apiManager)
        {
            Title = "Login";
            _identityService = identityService;
            _settingsService = settingsService;
            var url = _identityService.CreateAuthorizationRequest();
            AuthUrl = url;
            ShowWebView = true;
            SelfHandleLoading = true;
        }

        public async Task NavigateAsync(string url)
        {
            Debug.WriteLine("{0} NEXTURL {1}", DateTime.Now.ToString(), url);
            var unescapedUrl = System.Net.WebUtility.UrlDecode(url);
            if (unescapedUrl.Contains(GlobalSetting.Instance.Callback))
            {
                var authResponse = new AuthorizeResponse(url);
                if (!string.IsNullOrWhiteSpace(authResponse.Code))
                {
                    _settingsService.AuthCode = authResponse.Code;
                    var userToken = await _identityService.GetTokenAsync(authResponse.Code);
                    string accessToken = userToken.AccessToken;
                    if (!string.IsNullOrWhiteSpace(accessToken))
                    {
                        _settingsService.AuthAccessToken = accessToken;
                        _settingsService.AuthIdToken = userToken.IdToken;
                        _settingsService.RefreshToken = userToken.RefreshToken;
                        await NavigationService.NavigateAsync("MainPage");
                    }
                    IsBusy = false;
                }
            }
        }
    }
}
