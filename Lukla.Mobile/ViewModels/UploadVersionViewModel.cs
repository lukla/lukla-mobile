﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Lukla.Mobile.Helpers;
using Lukla.Mobile.Models;
using Lukla.Mobile.Services.Obsolute;
using Newtonsoft.Json;
using Prism.Commands;
using Prism.Navigation;

namespace Lukla.Mobile.ViewModels
{
    public class UploadVersionViewModel : ViewModelBase
    {
        private IKeyboardHelper _keyboardHelper;
        public DelegateCommand Save { get; private set; }
        private string _binId { get; set; }
        private LLDocument _doc;
        private LLFile File { get; set; }
        public LLDocument Doc { get => _doc; set { SetProperty(ref _doc, value) ; } }

        private List<LLBin> _bins;
        public List<LLBin> Bins
        {
            get => _bins;
            set
            {
                SetProperty(ref _bins, value);
            }
        }
        private List<SecurityCode> _securityCodes;
        public List<SecurityCode> SecurityCodes
        {
            get => _securityCodes;
            set
            {
                SetProperty(ref _securityCodes, value);
            }
        }

        private List<LLAnchor> _anchors;
        public List<LLAnchor> Anchors
        {
            get => _anchors;
            set
            {
                SetProperty(ref _anchors, value, () =>
                {
                    HeightAnchorList = _anchors.Count * 70;
                });
            }
        }

        private SecurityCode _selectedSecurity;
        public SecurityCode SelectedSecurity
        {
            get => _selectedSecurity;
            set
            {
                SetProperty(ref _selectedSecurity, value);
            }
        }

        private UploadVersionParam _apiParams;
        public UploadVersionParam ApiParams
        {
            get => _apiParams;
            set
            {
                SetProperty(ref _apiParams, value);
            }
        }

        private int _heightAnchorList;
        public int HeightAnchorList { get => _heightAnchorList; set { SetProperty(ref _heightAnchorList, value); } }

        public UploadVersionViewModel(INavigationService navigationService, IApiManager _apiManager, IKeyboardHelper keyboardHelper)
            : base(navigationService, _apiManager)
        {
            _keyboardHelper = keyboardHelper;
            Title = "Upload version";
            Bins = new List<LLBin>();
            SecurityCodes = new List<SecurityCode>();
            Save = new DelegateCommand(() =>
            {
                _ = OnSave();
            });
            InitialRequest();
        }

        private async void InitialRequest()
        {
            await RunSafe(GetSecurityCodes());
            await RunSafe(GetAnchors());
        }

        public async Task<string> GetSecurityCodes()
        {
            var resp = await ApiManager.GetSecurityCodes();

            if (resp.Data != null)
            {
                var codes = resp.Data;
                SecurityCodes = codes.Items;
            }
            return resp.ErrorMessage;
        }

        public async Task<string> GetAnchors()
        {
            var resp = await ApiManager.GetAnchors();

            if (resp.Data != null)
            {
                ProcessAnchors(resp.Data);
            }
            return resp.ErrorMessage;
        }

        public void ProcessAnchors(List<LLAnchor> anchors)
        {
            anchors = anchors.OrderBy(e => e.DisplayOrder).ToList();
            anchors = anchors.FindAll(e => e.UpdatedValueRequiredForNewVersion == true);
            anchors.ForEach(anchor =>
            {
                if (anchor.Type == 0 && anchor.DefaultValue != null)
                {
                    anchor.Value = anchor.DefaultValue;
                }
            });
            Anchors = anchors;
        }

        public bool ValidateInputs()
        {
            var isValid = true;
            Anchors.ForEach(anchor =>
            {
                if (anchor.UpdatedValueRequiredForNewVersion && !Utils.IsValidFormValue(anchor.Value))
                {
                    isValid = false;
                    return;
                }
            });
            return isValid;
        }

        public async Task OnSave()
        {
            _keyboardHelper.HideKeyboard();
            if (!ValidateInputs())
            {
                await App.Current.MainPage.DisplayAlert("", "Please enter valid values for the required fields", "OK");
                return;
            }
            var anchorsDict  = Utils.ToArrayAnchorDict(Anchors);
            var anchorsJson = JsonConvert.SerializeObject(anchorsDict);
            // create parameters
            var uploadParams = new UploadVersionParam
            {
                FileName = File.FileName,
                DocumentId = Doc.Id,
                File = File.Stream,
                Anchors = anchorsJson
            };
            var success = await RunSafeWithResult(RequestUploadVersion(uploadParams));
            if (success)
            {
                await App.Current.MainPage.DisplayAlert("", "Upload success!", "Ok");
                var navigationParams = new NavigationParameters();
                navigationParams.Add("RefreshDocList", true);
                await NavigationService.GoBackAsync(navigationParams);
            }
        }

        public async Task<string> RequestUploadVersion(UploadVersionParam p)
        {
            var resp = await ApiManager.UploadVersion(p);
            return resp.ErrorMessage;
        }

        override public void OnNavigatedTo(INavigationParameters parameters)
        {
            if (parameters.ContainsKey("Doc") && parameters.ContainsKey("BinId"))
            {
                Doc = (LLDocument)parameters["Doc"];
                _binId = (string)parameters["BinId"];
                File = (LLFile)parameters["File"];
            }
        }

        override public void OnNavigatedFrom(INavigationParameters parameters)
        {
            Console.WriteLine("OnNavigatedFrom");
        }
    }
}
