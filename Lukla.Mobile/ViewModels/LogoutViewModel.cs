﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows.Input;
using Lukla.Mobile.Services.Identity;
using Lukla.Mobile.Services.Obsolute;
using Lukla.Mobile.Services.Settings;
using Prism.Navigation;
using Xamarin.Forms;

namespace Lukla.Mobile.ViewModels
{
    public class LogoutViewModel : ViewModelBase
    {
        private string _logoutUrl;

        private ISettingsService _settingsService;
        private IIdentityService _identityService;
        public ICommand NavigateCommand => new Command<string>(async (url) => await NavigateAsync(url));
        public string LogoutUrl
        {
            get => _logoutUrl;
            set
            {
                SetProperty(ref _logoutUrl, value);
            }
        }

        public LogoutViewModel(
        ISettingsService settingsService,
        IIdentityService identityService,
        IApiManager _apiManager,
        INavigationService navigationService)
        : base(navigationService, _apiManager)
        {
            Title = "Login";
            _identityService = identityService;
            _settingsService = settingsService;

            var token = _settingsService.AuthIdToken;
            var url = _identityService.CreateLogoutRequest(token);
            LogoutUrl = url;
            SelfHandleLoading = true;
            IsBusy = true;
        }

        //  TODO: I'm not sure what the purpose of this is? how are you actually logging out?
        private async Task NavigateAsync(string url)
        {
            var unescapedUrl = System.Net.WebUtility.UrlDecode(url);
            if (CheckIsLogoutCallBackUrl(unescapedUrl))
            {
                _settingsService.ClearTokens();
                // go back to login
                await NavigationService.NavigateAsync("/NavigationPage/Authen");
            }
        }

        //  TODO: This is smelly, review.
        private bool CheckIsLogoutCallBackUrl(string url)
        {
            Debug.WriteLine("{0} NEXTURL: {1}", DateTime.Now.ToShortTimeString(), url);
            if (url.Length >= GlobalSetting.Instance.LogoutCallback.Length)
            {
                var logoutCallback = url.Substring(0, GlobalSetting.Instance.LogoutCallback.Length);
                return logoutCallback == GlobalSetting.Instance.LogoutCallback;
            } else
            {
                return false;
            }
        }
    }
}
