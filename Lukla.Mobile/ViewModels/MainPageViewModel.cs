﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Lukla.Mobile.Models;
using Lukla.Mobile.Services.Obsolute;
using Lukla.Mobile.Services.Settings;
using Plugin.FilePicker;
using Prism.Commands;
using Prism.Navigation;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Extended;

namespace Lukla.Mobile.ViewModels
{
    public class MainPageViewModel : ViewModelBase
    {
        public DelegateCommand LoadDocs { get; private set; }
        public DelegateCommand NavigateToSetting { get; private set; }
        public DelegateCommand<object> DownloadDocs { get; private set; }
        public DelegateCommand<object> EditDocs { get; private set; }
        public DelegateCommand CloneDocs { get; private set; }
        public DelegateCommand<object> UpdateDocs { get; private set; }
        public DelegateCommand<object> TappedCell { get; private set; }

        public DocumentSearchParams DocParams { get; private set; }
        private List<LLBin> _bins;
        public List<LLBin> Bins { get => _bins; set { SetProperty(ref _bins, value); } }

        private ISettingsService _settingsService;
        public ISettingsService SettingsService { get => _settingsService; }

        public InfiniteScrollCollection<LLDocument> Docs { get; }

        private string _searchKeyword;
        public string SearchKeyword { get => _searchKeyword; set { SetProperty(ref _searchKeyword, value); } }

        private bool _isLoadingMore;
        public bool IsLoadingMore { get => _isLoadingMore; set { SetProperty(ref _isLoadingMore, value); } }

        private bool _isLoadDocsFull;
        public bool IsLoadDocsFull { get => _isLoadDocsFull; set { SetProperty(ref _isLoadDocsFull, value); } }

        private bool _isShowActionMenu;
        public bool IsShowActionMenu { get => _isShowActionMenu; set { SetProperty(ref _isShowActionMenu, value); } }

        private int _totalDocs;
        public int TotalDocs { get => _totalDocs; set { SetProperty(ref _totalDocs, value); } }

        private LLBin _selectedBin;
        public LLBin SelectedBin
        {
            get => _selectedBin;
            set
            {
                SetProperty(ref _selectedBin, value, () =>
                {
                    if (_selectedBin != null)
                    {
                        Console.WriteLine("Selected bin changing to {0}", value.Name);
                        DocParams.Reset();
                        DocParams.BinId = value.Id;
                        SearchKeyword = "";
                        FetchDocs();
                    }
                    else
                    {
                        SearchKeyword = "";
                    }
                });
            }
        }

        private LLDocument _selectedDocument;
        public LLDocument SelectedDocument
        {
            get => _selectedDocument;
            set
            {
                if (_selectedDocument != value)
                {
                    Console.WriteLine("SelectedDocument");
                    SetProperty(ref _selectedDocument, value, () =>
                    {
                        IsShowActionMenu = true;
                    });
                }
                else
                {
                    IsShowActionMenu = !IsShowActionMenu;
                }
            }
        }


        public MainPageViewModel(ISettingsService settingsService,
            INavigationService navigationService,
            IApiManager _apiManager)
            : base(navigationService, _apiManager)
        {
            Title = "LUKLA";
            _settingsService = settingsService;
            _settingsService.NeedRefeshBins = false;
            LoadDocs = new DelegateCommand(async () =>
            {
                if (SelectedBin == null)
                {
                    await PageDialog.AlertAsync("Please select a bin", "Error", "OK");
                    return;
                }
                await RunSafe(GetDocs());
            });
            NavigateToSetting = new DelegateCommand(async () =>
                await NavigationService.NavigateAsync("Setting")
            );
            Bins = new List<LLBin>();
            Docs = new InfiniteScrollCollection<LLDocument>();
            DocParams = new DocumentSearchParams();
            IsLoadingMore = false;
            IsLoadDocsFull = false;
            IsShowActionMenu = false;

            Docs = new InfiniteScrollCollection<LLDocument>
            {
                OnLoadMore = async () =>
                {
                    Console.WriteLine("OnLoadMore");
                    DocParams.Page += 1;
                    IsLoadingMore = true;
                    var docsResponse = await ApiManager.GetDocuments(DocParams);
                    IsLoadingMore = false;
                    if (docsResponse.Data != null)
                    {
                        var docs = docsResponse.Data;
                        IsLoadDocsFull = docs.Items.Count < DocParams.PageSize;
                        Console.WriteLine("Load More Docs: {0} docs", docs.Items.Count);
                        return new InfiniteScrollCollection<LLDocument>(docs.Items);
                    }
                    else
                    {
                        await PageDialog.AlertAsync(docsResponse.ErrorMessage ?? Strings.GeneralError, "Error", "OK");
                        return new InfiniteScrollCollection<LLDocument>();
                    }
                },
                OnCanLoadMore = () => !IsLoadDocsFull
            };

            // Actions

            //DownloadDocs = new DelegateCommand<object>(async (sender) => await OnDownoadDocsAsync(sender));
            //UpdateDocs = new DelegateCommand<object>(async (sender) => await OnUpdateDocs(sender));
            //EditDocs = new DelegateCommand<object>(async (sender) => await OnEditDocs(sender));
            //TappedCell = new DelegateCommand<object>(sender => OnTappedCell(sender));
        }

        public async Task InitialRequests()
        {
            await RunSafe(GetBins());
            await GetProfile();
        }

        private async void FetchDocs()
        {
            await RunSafe(GetDocs());
        }

        public async Task<string> GetBins()
        {
            var binsResponse = await ApiManager.GetBins();
            _settingsService.NeedRefeshBins = false;
            if (binsResponse.Data != null)
            {
                var bins = binsResponse.Data;
                Bins = bins.Items;
                Console.WriteLine("Loaded Bins: {0} bins", Bins.Count);

                // clear docs
                if (Docs.Count > 0)
                {
                    Docs.Clear();
                }
            }
            return binsResponse.ErrorMessage;
        }

        public async Task<string> GetDocs()
        {
            DocParams.SearchTerm = SearchKeyword;
            var docsResponse = await ApiManager.GetDocuments(DocParams);

            if (docsResponse.Data != null)
            {
                var items = docsResponse.Data.Items;
                IsLoadDocsFull = items.Count < DocParams.PageSize;
                var data = docsResponse.Data;
                Docs.Clear();
                Docs.AddRange(data.Items);
                TotalDocs = data.FilteredItems;
                Console.WriteLine("Loaded Docs: {0} docs", Docs.Count);
            }
            else
            {
                return docsResponse.ErrorMessage ?? Strings.GeneralError;
            }
            return null;
        }

        // Actions

        public void OnSelectDoc(LLDocument doc)
        {
            var messageAndroid = Device.RuntimePlatform == Device.Android ? $" ({doc.FileName})" : "";
            var actionConfig = new ActionSheetConfig()
            {
                Title = $"{doc.Title}{messageAndroid}",
                Message = doc.FileName,
                UseBottomSheet = true,
                Cancel = new ActionSheetOption("Cancel")
            };

            // Display
            actionConfig.Add("Display", async () => await OnDownoadDocsAsync(doc), "view");

            // Edit
            actionConfig.Add("Edit", async () => await OnEditDoc(doc), "edit");

            // Update
            actionConfig.Add("Upload Revision", async () => await OnUploadDoc(doc), "upload");

            // Clone
            actionConfig.Add("Clone", async () => await OnCloneDocs(doc), "clone");
            UserDialogs.Instance.ActionSheet(actionConfig);
        }

        private async Task OnDownoadDocsAsync(LLDocument doc)
        {
            IsBusy = true;
            var file = await ApiManager.DownloadDoc(doc.VersionId);
            if (file.Data != null)
            {
                var stream = file.Data;
                string fileName = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), doc.FileName);

                using (var fileStream = new FileStream(fileName, FileMode.OpenOrCreate, FileAccess.Write))
                {
                    await stream.CopyToAsync(fileStream);
                }
                IsBusy = false;
                await Share.RequestAsync(new ShareFileRequest
                {
                    File = new ShareFile(fileName),
                    Title = "Save file"
                });
            }
            else
            {
                IsBusy = false;
                await App.Current.MainPage.DisplayAlert("Error", file.ErrorMessage ?? Strings.GeneralError, "Ok");
            }
        }

        private async Task OnEditDoc(LLDocument doc)
        {
            var navigationParams = new NavigationParameters
            {
                { "Doc", doc },
                {"BinId", _selectedBin.BinId }
            };
            await NavigationService.NavigateAsync("EditDocInfo", navigationParams);
        }

        private async Task OnCloneDocs(LLDocument doc)
        {
            var file = await CrossFilePicker.Current.PickFile();
            if (file != null)
            {
                var fileInfo = new LLFile
                {
                    FileName = file.FileName,
                    Stream = file.GetStream()
                };

                var navigationParams = new NavigationParameters
                {
                    { "Doc", doc },
                    {"BinId", _selectedBin.BinId },
                    { "Bins", Bins },
                    {"File", fileInfo }
                };
                await NavigationService.NavigateAsync("CloneDocument", navigationParams);
            }
        }

        private async Task OnUploadDoc(LLDocument doc)
        {
            var file = await CrossFilePicker.Current.PickFile();
            if (file != null)
            {
                var fileInfo = new LLFile
                {
                    FileName = file.FileName,
                    Stream = file.GetStream()
                };
                
                var navigationParams = new NavigationParameters
                {
                    { "Doc", doc },
                    {"BinId", _selectedBin.BinId },
                    {"File", fileInfo }
                };
                await NavigationService.NavigateAsync("UploadVersion", navigationParams);
            }
        }

        private void OnTappedCell(object sender)
        {
            var menuBar = sender as Grid;
            var cell = menuBar.Parent.Parent as ViewCell;
            if (cell != null)
            {
                menuBar.HeightRequest = menuBar.Height == 0 ? 50 : 0;
                Console.WriteLine(menuBar.Height == 0 ? 50 : 0);
                cell.ForceUpdateSize();
            }
            else
            {
                Console.WriteLine("Null cell");
            }
        }

        private async Task GetProfile()
        {
            var response = await ApiManager.GetProfile();
            if (response.Data != null)
            {
                _settingsService.UserInfo = response.Data;
            }
        }

        override public void OnNavigatedTo(INavigationParameters parameters)
        {
            Console.WriteLine("OnNavigatedTo");
            if (parameters.ContainsKey("RefreshDocList"))
            {
                FetchDocs();
            }
        }
    }
}
