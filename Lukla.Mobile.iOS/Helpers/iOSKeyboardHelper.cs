﻿using Lukla.Mobile.Helpers;
using UIKit;

namespace Lukla.iOS.Helpers
{
    public class iOSKeyboardHelper : IKeyboardHelper
    {
        public void HideKeyboard()
        {
            UIApplication.SharedApplication.KeyWindow.EndEditing(true);
        }
    }
}
