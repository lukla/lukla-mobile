﻿using Lukla.iOS.CustomControls;
using Lukla.Mobile.CustomRenderer;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(SelectableCell), typeof(SelectableCellRenderer))]
namespace Lukla.iOS.CustomControls
{
    public class SelectableCellRenderer: ViewCellRenderer
    {
        private UIView bgView;
        public override UITableViewCell GetCell(Cell item, UITableViewCell reusableCell, UITableView tv)
        {
            var cell = base.GetCell(item, reusableCell, tv);
            cell.BackgroundColor = UIColor.Black;
            cell.TextLabel.TextColor = UIColor.White;
            if (bgView == null)
            {
                bgView = new UIView(cell.SelectedBackgroundView.Bounds);
                bgView.Layer.BackgroundColor = UIColor.Blue.CGColor;
                bgView.Layer.BorderColor = UIColor.Yellow.CGColor;
                bgView.Layer.BorderWidth = 2.0f;
            }

            cell.SelectedBackgroundView = bgView;
            return cell;
        }
    }
}
