﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using DryIoc;
using FluentAssertions;
using Lukla.Mobile.Services.Mediator;
using Lukla.Mobile.Services.Mediator.Authentication;
using Lukla.Mobile.Services.Mediator.Behaviors;
using MediatR;
using Moq;
using Refit;
using Xunit;
using Xunit.Categories;

namespace Lukla.Mobile.Tests.Behaviors
{
    public class BehaviorTests
    {
        [Fact]
        [UnitTest]
        public async Task Resilient_Behavior()
        {
            //  ARRANGE
            var behavior = new ResilientBehavior<TestCommand, TestResponse>();
            var command = new TestCommand();
            var handler = new Mock<RequestHandlerDelegate<TestResponse>>();
            handler.Setup(x => x.Invoke()).Throws(new WebException("Failed"));

            //  ACT
            Func<Task> act = async () =>
            {
                await behavior.Handle(command, CancellationToken.None, handler.Object);
            };
            
            //  ASSERT
            await act.Should().ThrowAsync<WebException>();
            handler.Verify(x => x.Invoke(), Times.Exactly(6));
        }

        [Fact]
        [UnitTest]
        public async Task Token_Behavior()
        {
            //  ARRANGE
            var mediator = new Mock<IMediator>();
            mediator.Setup(m => m.Send(
                It.IsAny<StartSession.Command>(),
                It.IsAny<CancellationToken>()));

            var behavior = new TokenBehavior<TestCommand, TestResponse>(mediator.Object);
            var command = new TestCommand();
            var handler = new Mock<RequestHandlerDelegate<TestResponse>>();
            var exception = await ApiException.Create(new HttpRequestMessage(), HttpMethod.Get,
                new HttpResponseMessage(HttpStatusCode.Unauthorized));
            handler.Setup(x => x.Invoke()).Throws(exception);

            //  ACT
            Func<Task> act = async () =>
            {
                await behavior.Handle(command, CancellationToken.None, handler.Object);
            };

            //  ASSERT
            await act.Should().ThrowAsync<ApiException>();
            mediator.Verify(x => x.Send(It.IsAny<StartSession.Command>(), It.IsAny<CancellationToken>()), Times.Once);
        }

        [Fact]
        [IntegrationTest]
        public async Task Api_Call_Bad_Exception_Fails()
        {
            //  ARRANGE
            var action = new Mock<ITestAction>();
            action.Setup(x => x.Action()).Throws<Exception>();

            var container = Setup(action);
            var mediator = container.Resolve<IMediator>();

            //  ACT
            Func<Task> act = async () =>
            {
                await mediator.Send(new TestCommand(), CancellationToken.None);
            };

            //  ASSERT
            await act.Should().ThrowAsync<Exception>();
        }

        [Fact]
        [IntegrationTest]
        public async Task Api_Call_Web_Exception_Passes()
        {
            //  ARRANGE
            var action = new Mock<ITestAction>();
            action.SetupSequence(x => x.Action())
                .Throws<WebException>()
                .Throws(await ApiException.Create(new HttpRequestMessage(), HttpMethod.Get, new HttpResponseMessage(HttpStatusCode.ServiceUnavailable)))
                .Throws<TaskCanceledException>()
                .Returns(Task.CompletedTask);

            var container = Setup(action);
            var mediator = container.Resolve<IMediator>();

            //  ACT
            var response = await mediator.Send(new TestCommand(), CancellationToken.None);

            //  ASSERT
            response.Should().NotBeNull();
        }

        [Fact]
        [IntegrationTest]
        public async Task Api_Call_Token_Expires_Passes()
        {
            //  ARRANGE
            var action = new Mock<ITestAction>();
            action.SetupSequence(x => x.Action())
                .Throws(await ApiException.Create(new HttpRequestMessage(), HttpMethod.Get, new HttpResponseMessage(HttpStatusCode.Unauthorized)))
                .Returns(Task.CompletedTask);

            var container = Setup(action);
            var mediator = container.Resolve<IMediator>();

            //  ACT
            var response = await mediator.Send(new TestCommand(), CancellationToken.None);

            //  ASSERT
            response.Should().NotBeNull();
        }
        

        private IContainer Setup(Mock<ITestAction> action)
        {
            var container = new Container()
                .RegisterMediator();
            container.RegisterInstance(action.Object);
            container.RegisterMany(typeof(TestCommandHandler).GetAssembly().GetTypes().Where(t => t.IsMediatorHandler()));
            return container;
        }
    }

    public class TestCommand :
        IRequest<TestResponse>,
        IResilientAction,
        ITokenAction {}

    public class TestCommandHandler : IRequestHandler<TestCommand, TestResponse>
    {
        private readonly ITestAction _testAction;
        public TestCommandHandler(ITestAction testAction)
        {
            _testAction = testAction;
        }

        public async Task<TestResponse> Handle(TestCommand request, CancellationToken cancellationToken)
        {
            await _testAction.Action();
            return new TestResponse()
            {
                Id = Guid.NewGuid()
            };
        }
    }

    public class TestResponse
    {
        public Guid Id { get; set; }
    }

    public interface ITestAction
    {
        Task Action();
    }
}
