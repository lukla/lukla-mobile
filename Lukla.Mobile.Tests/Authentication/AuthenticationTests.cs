﻿using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using AutoBogus;
using Bogus;
using FluentAssertions;
using Fusillade;
using Lukla.Mobile.Services;
using Lukla.Mobile.Services.Api;
using Lukla.Mobile.Services.Mediator.Authentication;
using MediatR;
using Microsoft.Extensions.Logging;
using Moq;
using Newtonsoft.Json;
using RichardSzalay.MockHttp;
using Xamarin.Essentials;
using Xunit;
using Xunit.Categories;

namespace Lukla.Mobile.Tests.Authentication
{
    public class AuthenticationTests
    {
        [Fact]
        [UnitTest]
        public async Task Start_Session_Succeeds()
        {
            //  ARRANGE
            var logger = new Mock<ILogger<StartSession.Handler>>();
            var mediator = new Mock<IMediator>();
            var session = new Mock<ICurrentSession>();

            var codeResponse = AutoFaker.Generate<GetAuthenticationCode.Response>();
            var tokenResponse = AutoFaker.Generate<GetToken.Response>();

            mediator.Setup(x => x.Send(It.IsAny<GetAuthenticationCode.Request>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(codeResponse);
            mediator.Setup(x => x.Send(It.Is<GetToken.Request>(v => v.Code == codeResponse.Code && v.CodeVerification == codeResponse.Verifier), It.IsAny<CancellationToken>()))
                .ReturnsAsync(tokenResponse);

            var handler = new StartSession.Handler(logger.Object, mediator.Object, session.Object);
            var request = new StartSession.Command();

            //  ACT
            await handler.Handle(request, CancellationToken.None);

            //  ASSERT
            session.Verify(x => x.Set(It.Is<ISession>(v => v.AccessToken == tokenResponse.AccessToken)));
        }

        [Fact]
        [UnitTest]
        public async Task Get_Authentication_Code_Succeeds()
        {
            //  ARRANGE
            var authenticator = new Mock<IAuthenticator>();
            var settings = AutoFaker.Generate<AuthenticationSettings>();
            var logger = new Mock<ILogger<GetAuthenticationCode.Handler>>();

            var code = "asdfasdfasdf";
            var authResponse = AutoFaker.Generate<WebAuthenticatorResult>();
            authResponse.Properties.Add("code", code);
            authenticator.Setup(x => x.Authenticate(It.IsAny<Uri>(), It.IsAny<Uri>())).ReturnsAsync(authResponse);

            var handler = new GetAuthenticationCode.Handler(settings, authenticator.Object, logger.Object);
            var request = new GetAuthenticationCode.Request();

            //  ACT
            var response = await handler.Handle(request, CancellationToken.None);

            //  ASSERT
            response.Should().NotBeNull();
            response.Code.Should().Be(code);
            response.Verifier.Should().NotBeNull();
        }

        [Fact]
        [UnitTest]
        public async Task Get_Token_Succeeds()
        {
            //  ARRANGE
            var settings = AutoFaker.Generate<AuthenticationSettings>();
            var logger = new Mock<ILogger<GetToken.Handler>>();
            var factory = new Mock<IMobileApiFactory>();

            var httpContent = AutoFaker.Generate<GetToken.Response>();
            var mockHttp = new MockHttpMessageHandler();
            mockHttp.When(settings.TokenEndpoint.AbsoluteUri).Respond("application/json", JsonConvert.SerializeObject(httpContent));
            factory.Setup(x => x.Create(It.IsAny<Priority>())).Returns(mockHttp.ToHttpClient);

            var handler = new GetToken.Handler(settings, logger.Object, factory.Object);
            var request = new GetToken.Request();

            //  ACT
            var response = await handler.Handle(request, CancellationToken.None);

            //  ASSERT
            response.Should().NotBeNull();
            response.Should().BeEquivalentTo(httpContent);
        }

        [Fact]
        [UnitTest]
        public async Task End_Session_Succeeds()
        {
            //  ARRANGE
            var logger = new Mock<ILogger<EndSession.Handler>>();
            var mediator = new Mock<IMediator>();
            var session = new Mock<ICurrentSession>();

            session.Setup(x => x.Get(It.IsAny<CancellationToken>())).ReturnsAsync(new Session()
            {
                AccessToken = new Faker().Random.AlphaNumeric(15)
            });

            var handler = new EndSession.Handler(session.Object, mediator.Object, logger.Object);
            var request = new EndSession.Command();

            //  ACT
            await handler.Handle(request, CancellationToken.None);

            //  ASSERT
            mediator.Verify(x => x.Send(It.IsAny<RevokeToken.Command>(), It.IsAny<CancellationToken>()));
            session.Verify(x => x.Clear());
        }

        [Fact]
        [UnitTest]
        public async Task Revoke_Token_Succeeds()
        {
            //  ARRANGE
            var settings = AutoFaker.Generate<AuthenticationSettings>();
            var factory = new Mock<IMobileApiFactory>();

            var mockHttp = new MockHttpMessageHandler();
            var revokeRequest = mockHttp.When(settings.RevokeEndpoint.AbsoluteUri).Respond(HttpStatusCode.OK);
            factory.Setup(x => x.Create(It.IsAny<Priority>())).Returns(mockHttp.ToHttpClient);

            var handler = new RevokeToken.Handler(factory.Object, settings);
            var request = new RevokeToken.Command(new Faker().Random.AlphaNumeric(15));

            //  ACT
            await handler.Handle(request, CancellationToken.None);

            //  ASSERT
            mockHttp.GetMatchCount(revokeRequest).Should().Be(1);
        }
    }
}
