## Lukla Xamarin

The mobile version of [Lukla.co]()

![app_screenshots](./doc_pics/app_screenshots.png)

## Setup

- IDE: Install Visual Studio(VS) by following https://docs.microsoft.com/en-us/xamarin/get-started/installation/?pivots=macos
- XCode 11.3 or later. https://apps.apple.com/us/app/xcode/id497799835?mt=12
- Android Studio with Android 6.0 / API level 23 or later. https://developer.android.com/studio
- To build, debug, and sign iOS app on VS Window you have to connect to Mac buid host https://docs.microsoft.com/en-us/xamarin/ios/get-started/installation/windows/connecting-to-mac/

## Project structure

This project was created by Prism Template Pack (https://prismlibrary.com/) with Prism framework that supports Xamarin.Forms and provides an implementation of a collection of design patterns: MVVM, dependency injection, commands, EventAggregator, and others.

There are 3 projects on Lukla solution:

![solution.png](./doc_pics/solution.png)

- Lukla: A .NET Standard Library project that contains the code that will be shared among the other targeting platforms. Here we will write our business logic and shared code.
- Lukla.Android/Lukla.iOS: Custom renderer classes, assets, build configurations, ... for Android/ iOS.

## Deploy

![run.png](./doc_pics/run.png)

Select platform > build configuration > simulator/device.

**Run on device:** Plug your iOS/Android device in then you can select it from the device list to run (*)

**Run on iOS simulator**: You can run on all of simulators that is managed by Xcode.

**Run on Android simulator**: Use VS/Manage Android Devices or Android Studio/AVD Manager to manage your Android simulator.

![Screen Shot 2020-03-31 at 12.46.33 AM](./doc_pics/adv.png)

(*) Need to setup provisioning for Mac to run app on physical device: https://docs.microsoft.com/en-us/xamarin/ios/get-started/installation/device-provisioning/

## How to

#### 1. Add new screen

* Create view model class: Lukla/ViewModels/<ScreenName>ViewModel.cs

  ```c#
  public class <ScreenName>ViewModel : ViewModelBase
  ```

* Create view xaml (right click on Views and go to Add/New File/Forms/Forms ContentPage XAML): Lukla/Views/<ScreenName>.xaml.

* Register screen to navigation service: Open App.xml.cs and add this line

  ```c#
  				protected override void RegisterTypes(IContainerRegistry containerRegistry)
          {
            ----------
              containerRegistry.RegisterForNavigation<{ScreenName}, {ScreenName}ViewModel>();
          }
  ```

  

* Optional: If this screen has models class, put it into Lukla/Models.

#### 2. Navigating between screens

- Push (navigate to new screen)

  `await NavigationService.NavigateAsync("<ScreenName>")`

- Back to previous screen

  `await NavigationService.GoBackAsync();`

- Reset navigation stack to a screen

  `await NavigationService.NavigateAsync("/NavigationPage/<ScreenName>");`

*Should handle all navigations on viewmodel classes.

#### 5. Add new api service.

All classes for API services is in Lukla/Services. There are two API services in ApiManager are `appApi` and `indexAPI` corresponding to two endpoints:

- https://documents-beta.lukla.com: all most of APIs is here.
- https://index-beta.lukla.com: for searching documents.

Add new func to a API services (`appApi`/`indexAPI`):

* Add new function to `IAppApi`/`IIndexApi` like below

  ```c#
  [Post("/switch/companies/{companyId}")]
  Task<ApiResponse<T>> SwitchCompany<T>(string companyId);
  ```

* Add new func to `IApiManager` 

  ```c#
  Task<LLApiResponse<Stream>> DownloadDoc(string versionId);
  ```

* Implement above func in `ApiManager`

  ```c#
  				public async Task<LLApiResponse<Stream>> DownloadDoc(string versionId)
          {
              var cts = new CancellationTokenSource();
              var task = appApi.GetApi(Priority.UserInitiated).DownloadDoc<object>(versionId);
              runningTasks.Add(task.Id, cts);
              var response = await task;
              var result = new LLApiResponse<Stream>();
              if (response.IsSuccessStatusCode)
              {
                  result.Data = await response.Content.ReadAsStreamAsync();
              } else
              {
                  result.ErrorMessage = $"Error: {response.StatusCode.ToString()}";
              }
              return result;
          }
  ```

  

* Use API func

  ```c#
  var file = await ApiManager.DownloadDoc(doc.VersionId);
  if (file.Data != null)
  {
    --------          
  ```

#### 3. Import images

##### iOS

* Open Assets.xcassets from Lukla.iOS
* New Image Assets
* Drag images into 1x, 2x, 3x on Universal section.

For further support: https://docs.microsoft.com/en-us/xamarin/ios/app-fundamentals/images-icons/displaying-an-image?tabs=windows

##### Android

Place images (same name with iOS image asset) in the Resources/drawable directory with Build Action: AndroidResource.

##### Display images

* On XAML file.

  ```xaml
  <ToolbarItem
              IconImageSource="icon_settings.png"
              Command="{Binding NavigateToSetting}"
              Order="Primary"
              Priority="0" />
  ```

* On cs file.

  <!--add View menu item to action sheet-->

  ```c#
  actionConfig.Add("Display", async () => await OnDownoadDocsAsync(doc), "view")
  ```

#### 4. Create bindable property

```c#
				private <DataType> _propertyName;
        public <DataType> PropertyName
        {
            get => _propertyName;
            set
            {
                SetProperty(ref _propertyName, value, () =>
                {
                    // completion callback
                });
            }
        }
```



#### 5. Persist local data

```c#
				async Task AddOrUpdateValueInternal<T>(string key, T value)
        {
            if (value == null)
            {
                await Remove(key);
            }

            Application.Current.Properties[key] = value;
            try
            {
                await Application.Current.SavePropertiesAsync();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Unable to save: " + key, " Message: " + ex.Message);
            }
        }
```



#### 6. Distribute

##### 	6.1 - To Testflight/Appstore for iOS

* Increase build number or version in Info.plist.

  ![change_build_number](./doc_pics/change_build_number.png)

* Right click on Lukla.IOS, select Archive for Publishing

* Select a build, then Sign and Distribute.

##### 	6.2 - Generate signed APK for Android

* Increase build number or version in Android.Android/Options/Android Application

  ![android_build](./doc_pics/android_build.png)

* Right click on Lukla.IOS, select Archive for Publishing

* Select a build, then Sign and Distribute, select Adhoc.

* Create a new key or select a key if exist, then Next->Publish.

## References

https://xamgirl.com/prism-in-xamarin-forms-step-by-step-part-1/

https://docs.microsoft.com/en-us/xamarin/ios/get-started/installation/device-provisioning/

https://docs.microsoft.com/en-us/xamarin/xamarin-forms/

https://docs.microsoft.com/en-us/xamarin/android/deploy-test/publishing/

## Contact

Mr. Thi Vo - thi@poetadigital.com

